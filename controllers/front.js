var config = require("../config/config"),
	env = process.env.NODE_ENV || 'development',
	configEnv = config[env],
	front = {
		config: require("../config/front")
	},
	utils = require('../lib/utils');

var Mongoose = require('mongoose'),
	//Schema = Mongoose.Schema.
	//User = Mongoose.model('User'),
	Serial = Mongoose.model('Serial'),
	Episode = Mongoose.model('Episode'),
	EpisodeViews = Mongoose.model('EpisodeViews');


exports.index = function(req, res) {
	res.header('Cache-Control', 'public, max-age=' + config.cache.maxAge);

	var thisPage = front.config.pages.index;

	var page = {
		page: thisPage,
		pages: {
			anime: front.config.pages.anime,
			episode: front.config.pages.episode
		},
		originalUrl: req.originalUrl,
		title: thisPage.title,
		isHome: true,
		description: thisPage.description,
		hasFooter: true,
		isAjax: req.query.ajax ? true : false,
		socialImage: utils.getDefaultSocialImage(),
		partners: utils.getPartners()
	};
	page.bodyClass = '';

	page.styles = utils.getFrontStyles([]);
	if ( page.isAjax ) {
		page.scripts = utils.getFrontAjaxScripts([]);
	} else {
		page.scripts = utils.getFrontScripts([]);
	}

	page.topMenu = front.config.topMenu;

	var objEpisodes = new Episode();
	var firstPage = 1;
	//objEpisodes.getHighlightedPaged(firstPage, config.smallList.perPage, function(err, highlightedEpisodes) {
	objEpisodes.getReleasesPaged(firstPage, config.smallList.perPage, function(err, releasedEpisodes) {
		if (err) {
			//return utils.redirect(res, front.config.pages.notFound.url);
			return utils.notFound(req, res);
		}

		//console.log(page.episodeUrl.sprintf(weekPopularItem._owner.slug, weekPopularItem.slug));

		objEpisodes.getRecentsPaged(firstPage, config.smallList.perPage, function(err, recentItems) {
			if (err) {
				//return utils.redirect(res, front.config.pages.notFound.url);
				return utils.notFound(req, res);
			}

			var renderObject = {
				config: config,
				utils: utils,
				page: page,
				hasReleasedEpisodes: releasedEpisodes.length,
				releasedEpisodes: releasedEpisodes,
				hasRecentItems: recentItems.length,
				recentItems: recentItems
			};

			res.render(thisPage.viewPath, renderObject);
		});
	});
}

exports.login = function(req, res) {
	if (utils.isAdminLoggedIn(req)) {
		var admin = {
			config: require('../config/admin')
		};
		utils.redirect(res, admin.config.pages.dashboard.url);
	} else {
		var thisPage = front.config.pages.login;

		var page = {
			page: thisPage,
			originalUrl: req.originalUrl,
			title: thisPage.title,
			description: thisPage.description,
			hasFooter: false,
			isAjax: req.query.ajax ? true : false
		};
		page.bodyClass = '';

		page.styles = utils.getFrontStyles([
			{ "href": "login.css" }
		], false);
		page.scripts = utils.getFrontScripts([
			{ "src": "login.js" },
		], false);

		var form = {
			ajaxAction: 'admin-login',
			redirectTo: req[configEnv.sessionConfig.cookieName].redirectAfterLogin
		};

		res.render(thisPage.viewPath, {
			config: config,
			utils: utils,
			form: form,
			page: page
		});
	}
}

exports.doar = function(req, res) {
	res.header('Cache-Control', 'public, max-age=' + config.cache.maxAge);

	var thisPage = front.config.pages.doar;

	var page = {
		page: thisPage,
		originalUrl: req.originalUrl,
		title: thisPage.title,
		description: thisPage.description,
		hasFooter: true,
		isAjax: req.query.ajax ? true : false
	};
	page.bodyClass = 'doar';

	page.styles = utils.getFrontStyles([]);
	page.scripts = utils.getFrontScripts([]);

	page.topMenu = front.config.topMenu;

	page.breadcrumb = [
		{
			title: front.config.pages.index.title,
			url: front.config.pages.index.url
		},
		{
			title: page.title
		}
	];

	var donation = require('../config/donations');

	res.render(thisPage.viewPath, {
		config: config,
		utils: utils,
		page: page,
		donation: donation
	});
}

exports.animeList = function(req, res) {
	res.header('Cache-Control', 'public, max-age=' + config.cache.maxAge);

	var thisPage = front.config.pages.animes;

	var paginationPage = req.params.page ? req.params.page : 1;

	var page = {
		page: thisPage,
		pages: {
			anime: front.config.pages.anime
		},
		originalUrl: req.originalUrl,
		title: thisPage.title,
		isHome: false,
		description: thisPage.description,
		hasFooter: true,
		isAjax: req.query.ajax ? true : false,
		reqUrl: req.protocol + '://' + req.get('host') + req.originalUrl,
		socialImage: utils.getDefaultSocialImage(),
		partners: utils.getPartners()
	};
	page.bodyClass = 'anime-list';

	page.styles = utils.getFrontStyles([]);
	if ( page.isAjax ) {
		page.scripts = utils.getFrontAjaxScripts([]);
	} else {
		page.scripts = utils.getFrontScripts([]);
	}

	page.topMenu = front.config.topMenu;

	var objSerial = new Serial();
	var searchItemsPerPage = config.xBigList.perPage;
	objSerial.getAllOrdererByTitle(paginationPage, searchItemsPerPage, function(err, rSerials) {
		if (err) {
			//return utils.redirect(res, front.config.pages.notFound.url);
			return utils.notFound(req, res);
		}

		page.breadcrumb = [
			{
				title: front.config.pages.index.title,
				url: front.config.pages.index.url
			},
			{
				title: page.title
			}
		];

		objSerial.count(function(err, length) {
			if (err) {
				//return utils.redirect(res, front.config.pages.notFound.url);
				return utils.notFound(req, res);
			}

			var lastPage = Math.ceil(length / searchItemsPerPage);
			var pagination = {
				page: parseInt(paginationPage),
				pageUrl: thisPage.urlPaged,
				lastPage: lastPage
			};

			var renderObject = {
				config: config,
				utils: utils,
				page: page,
				itemList: rSerials,
				pagination: pagination
			};

			res.render(thisPage.viewPath, renderObject);
		});
	});
}

exports.popularEpisodes = function(req, res) {
	res.header('Cache-Control', 'public, max-age=' + config.cache.maxAge);

	var thisPage = front.config.pages.popularEpisodes;

	var paginationPage = req.params.page ? req.params.page : 1;

	var page = {
		page: thisPage,
		pages: {
			anime: front.config.pages.anime,
			episode: front.config.pages.episode
		},
		originalUrl: req.originalUrl,
		title: thisPage.title,
		isHome: false,
		description: thisPage.description,
		hasFooter: true,
		isAjax: req.query.ajax ? true : false,
		reqUrl: req.protocol + '://' + req.get('host') + req.originalUrl,
		socialImage: utils.getDefaultSocialImage(),
		partners: utils.getPartners(),
		bodyClass: 'popular-episode-list',
		topMenu: front.config.topMenu
	};

	page.styles = utils.getFrontStyles([]);
	if ( page.isAjax ) {
		page.scripts = utils.getFrontAjaxScripts([]);
	} else {
		page.scripts = utils.getFrontScripts([]);
	}

	var objEpisodes = new Episode(),
		perPage = config.xBigList.perPage;
	objEpisodes.getSortedByViewsPaged(paginationPage, perPage, function(err, rEpisodes) {
		if (err) {
			//return utils.redirect(res, front.config.pages.notFound.url);
			return utils.notFound(req, res);
		}

		page.breadcrumb = [
			{
				title: front.config.pages.index.title,
				url: front.config.pages.index.url
			},
			{
				title: page.title
			}
		];

		objEpisodes.count(function(err, length) {
			if (err) {
				//return utils.redirect(res, front.config.pages.notFound.url);
				return utils.notFound(req, res);
			}

			var lastPage = Math.ceil(length / perPage);
			var pagination = {
				page: parseInt(paginationPage),
				pageUrl: thisPage.urlPaged,
				lastPage: lastPage
			};

			var renderObject = {
				config: config,
				utils: utils,
				page: page,
				itemList: rEpisodes,
				pagination: pagination
			};

			res.render(thisPage.viewPath, renderObject);
		});
	});
}

exports.highlightedEpisodes = function(req, res) {
	res.header('Cache-Control', 'public, max-age=' + config.cache.maxAge);

	var thisPage = front.config.pages.highlightedEpisodes;

	var paginationPage = req.params.page ? req.params.page : 1;

	var page = {
		page: thisPage,
		pages: {
			anime: front.config.pages.anime,
			episode: front.config.pages.episode
		},
		originalUrl: req.originalUrl,
		title: thisPage.title,
		isHome: false,
		description: thisPage.description,
		hasFooter: true,
		isAjax: req.query.ajax ? true : false,
		reqUrl: req.protocol + '://' + req.get('host') + req.originalUrl,
		socialImage: utils.getDefaultSocialImage(),
		partners: utils.getPartners(),
		bodyClass: 'highlighted-episode-list',
		topMenu: front.config.topMenu
	};

	page.styles = utils.getFrontStyles([]);
	if ( page.isAjax ) {
		page.scripts = utils.getFrontAjaxScripts([]);
	} else {
		page.scripts = utils.getFrontScripts([]);
	}

	var objEpisodes = new Episode(),
		perPage = config.xBigList.perPage;
	objEpisodes.getHighlightedPaged(paginationPage, perPage, function(err, rEpisodes) {
		if (err) {
			//return utils.redirect(res, front.config.pages.notFound.url);
			return utils.notFound(req, res);
		}

		page.breadcrumb = [
			{
				title: front.config.pages.index.title,
				url: front.config.pages.index.url
			},
			{
				title: page.title
			}
		];

		objEpisodes.countByHighlighted(function(err, length) {
			if (err) {
				//return utils.redirect(res, front.config.pages.notFound.url);
				return utils.notFound(req, res);
			}

			var lastPage = Math.ceil(length / perPage);
			var pagination = {
				page: parseInt(paginationPage),
				pageUrl: thisPage.urlPaged,
				lastPage: lastPage
			};

			var renderObject = {
				config: config,
				utils: utils,
				page: page,
				itemList: rEpisodes,
				pagination: pagination
			};

			res.render(thisPage.viewPath, renderObject);
		});
	});
}

exports.animeSingle = function(req, res) {
	res.header('Cache-Control', 'public, max-age=' + config.cache.maxAge);

	var slugAnime = req.params.slugAnime,
		paginationPage = req.params.page ? req.params.page : 1;

	var thisPage = front.config.pages.anime;


	var page = {
		page: thisPage,
		pages: {
			episode: front.config.pages.episode,
			anime: front.config.pages.anime
		},
		originalUrl: req.originalUrl,
		title: thisPage.title,
		isHome: false,
		description: thisPage.description,
		hasFooter: true,
		isAjax: req.query.ajax ? true : false,
		reqUrl: req.protocol + '://' + req.get('host') + req.originalUrl,
		partners: utils.getPartners()
	};
	page.bodyClass = 'anime';

	page.styles = utils.getFrontStyles([]);
	if ( page.isAjax ) {
		page.scripts = utils.getFrontAjaxScripts([]);
	} else {
		page.scripts = utils.getFrontScripts([]);
	}

	page.topMenu = front.config.topMenu;

	var objSerial = new Serial();
	objSerial.getBySlug(slugAnime, function(err, rSerial) {
		if (err || !rSerial) {
			return utils.notFound(req, res);
		}

		page.title = page.title.sprintf(rSerial.title);
		page.description = page.description.sprintf( utils.s.prune(rSerial.synopsis, 145) );
		page.socialImage = utils.getCoverUrl(rSerial.cover);

		page.breadcrumb = [
			{
				title: front.config.pages.index.title,
				url: front.config.pages.index.url
			},
			{
				title: front.config.pages.animes.title,
				url: front.config.pages.animes.url
			},
			{
				title: page.title
			}
		];

		var objEpisode = new Episode();
		var itemsPerPage = config.xTremeBigList.perPage;

		var findEpisodeOptions = {
			where: {
				status: true,
				_owner: rSerial._id,
				$or: [
					{ "type.normal": true },
					{ "type.filler": true }
				]
			}
		};
		objEpisode.getCustomPaged(findEpisodeOptions, paginationPage, itemsPerPage, function(err, rEpisodes) {
			if (err) {
				//return utils.redirect(res, front.config.pages.notFound.url);
				return utils.notFound(req, res);
			}

			var findEpisodeOptions = {
				where: {
					status: true,
					_owner: rSerial._id,
					$or: [
						{ "type.ova": true }
					]
				}
			};
			objEpisode.getCustomPaged(findEpisodeOptions, paginationPage, itemsPerPage, function(err, rOvas) {
				if (err) {
					//return utils.redirect(res, front.config.pages.notFound.url);
					return utils.notFound(req, res);
				}

				var findEpisodeOptions = {
					where: {
						status: true,
						_owner: rSerial._id,
						$or: [
							{ "type.movie": true }
						]
					}
				};
				objEpisode.getCustomPaged(findEpisodeOptions, paginationPage, itemsPerPage, function(err, rMovies) {
					if (err) {
						//return utils.redirect(res, front.config.pages.notFound.url);
						return utils.notFound(req, res);
					}

					var findEpisodeOptions = {
						where: {
							status: true,
							_owner: rSerial._id,
							$or: [
								{ "type.teaser": true }
							]
						}
					};
					objEpisode.getCustomPaged(findEpisodeOptions, paginationPage, itemsPerPage, function(err, rTeasers) {
						if (err) {
							//return utils.redirect(res, front.config.pages.notFound.url);
							return utils.notFound(req, res);
						}

						var objSerials = new Serial();
						objSerials.getByGenres(rSerial, 4, function(err, rSimilarAnimes) {
							if (err) {
								//return utils.redirect(res, front.config.pages.notFound.url);
								return utils.notFound(req, res);
							}

							var renderObject = {
								config: config,
								utils: utils,
								page: page,
								anime: rSerial,
								normalFillerEpisodes: rEpisodes,
								ovaEpisodes: rOvas,
								movieEpisodes: rMovies,
								teaserEpisodes: rTeasers,
								similarAnimes: rSimilarAnimes
							};

							res.render(thisPage.viewPath, renderObject);
						});
						
					});
				});
			});

		});
		
	});
}

exports.animeRandom = function(req, res) {
	
	var objSerial = new Serial();
	objSerial.getRandom(function(err, rRandomSerial) {
		if (err) {
			return utils.notFound(req, res);
		} else {
			var animeUrl = front.config.pages.anime.url.sprintf(rRandomSerial.slug);
			utils.redirect(res, animeUrl);
		}
	});
}

exports.episode = function(req, res) {
	//res.header('Cache-Control', 'public, max-age=' + config.cache.maxAge);

	var slugAnime = req.params.slugAnime,
		slugEpisode = req.params.slugEpisode;

	var thisPage = front.config.pages.episode;

	var page = {
		page: thisPage,
		pages: {
			anime: front.config.pages.anime,
			episode: front.config.pages.episode
		},
		originalUrl: req.originalUrl,
		title: thisPage.title,
		isHome: false,
		description: thisPage.description,
		hasFooter: true,
		isAjax: req.query.ajax ? true : false,
		//reqUrl: req.protocol + '://' + req.get('host') + req.originalUrl,
		reqUrl: configEnv.commentBaseUrl + req.originalUrl,
		cleanUrl: configEnv.commentBaseUrl + req._parsedUrl.pathname,
		partners: utils.getPartners()
	};
	page.bodyClass = 'episode';

	page.styles = utils.getFrontStyles([]);
	if ( page.isAjax ) {
		page.scripts = utils.getFrontAjaxScripts([]);
	} else {
		page.scripts = utils.getFrontScripts([]);
	}

	page.topMenu = front.config.topMenu;
	//page.titleUrl = front.config.pages.episode.url;

	var clientIp = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	var episodeView = {
		$push: {
			views: {
				ip: clientIp
			}
		}
		
	};

	var objEpisodes = new Episode();
	objEpisodes.getBySlugAddView(slugEpisode, episodeView, function(err, rEpisode) {
		if (err) {
			//return utils.redirect(res, front.config.pages.notFound.url);
			return utils.notFound(req, res);
		}

		rEpisode = rEpisode.length ? rEpisode[0] : false;

		page.title = page.title.sprintf(rEpisode.title);
		page.description = page.description.sprintf(rEpisode.title);
		page.socialImage = utils.getCoverUrl(rEpisode._owner.cover);

		page.breadcrumb = [
			{
				title: front.config.pages.index.title,
				url: front.config.pages.index.url
			},
			{
				title: front.config.pages.animes.title,
				url: front.config.pages.animes.url
			},
			{
				title: front.config.pages.anime.title.sprintf(rEpisode._owner.title),
				url: front.config.pages.anime.url.sprintf(rEpisode._owner.slug)
			},
			{
				title: page.title
			}
		];

		var otherEpisodesLength = config.xSmallList.perPage;
		objEpisodes.getNext(rEpisode, otherEpisodesLength, function(err, rNextEpisode) {
			if (err) {
				//return utils.redirect(res, front.config.pages.notFound.url);
				return utils.notFound(req, res);
			}

			objEpisodes.getPrev(rEpisode, otherEpisodesLength, function(err, rPrevEpisode) {
				if (err) {
					//return utils.redirect(res, front.config.pages.notFound.url);
					return utils.notFound(req, res);
				}

				var objSerials = new Serial();
				objSerials.getByGenres(rEpisode._owner, 4, function(err, rSimilarAnimes) {
					var prevStuff = {
						boxTitleText: '',
						episodes: rPrevEpisode,
						boxNoEpisodesText: ''
					};

					var nextStuff = {
						boxTitleText: '',
						episodes: rNextEpisode,
						boxNoEpisodesText: ''
					};

					if ( rEpisode.type.normal || rEpisode.type.filler ) {
						prevStuff.boxTitleText = 'Episódios Anteriores';
						prevStuff.boxNoEpisodesText = 'Não há episódios anteriores!';

						nextStuff.boxTitleText = 'Próximos Episódios';
						nextStuff.boxNoEpisodesText = 'Não há próximos episódios!';
					} else if ( rEpisode.type.ova ) {
						prevStuff.boxTitleText = 'OVAs Anteriores';
						prevStuff.boxNoEpisodesText = 'Não há OVAs anteriores!';

						nextStuff.boxTitleText = 'Próximos OVAs';
						nextStuff.boxNoEpisodesText = 'Não há próximas OVAs!';
					} else if ( rEpisode.type.movie ) {
						prevStuff.boxTitleText = 'Filmes Anteriores';
						prevStuff.boxNoEpisodesText = 'Não há filmes anteriores!';

						nextStuff.boxTitleText = 'Próximos Filmes';
						nextStuff.boxNoEpisodesText = 'Não há próximos filmes!';
					} else if ( rEpisode.type.teaser ) {
						prevStuff.boxTitleText = 'Teasers Anteriores';
						prevStuff.boxNoEpisodesText = 'Não há teasers anteriores!';

						nextStuff.boxTitleText = 'Próximos Teasers';
						nextStuff.boxNoEpisodesText = 'Não há próximos teasers!';
					}

					var renderObject = {	
						config: config,
						utils: utils,
						page: page,
						episode: rEpisode,
						prevStuff: prevStuff,
						nextStuff: nextStuff,
						similarAnimes: rSimilarAnimes,
						episodeViews: rEpisode.viewsCount + 1
					};

					res.render(thisPage.viewPath, renderObject);
				});

			});
		});

	});
}

exports.searchEpisode = function(req, res) {
	res.header('Cache-Control', 'public, max-age=' + config.cache.maxAge);

	var thisPage = front.config.pages.searchEpisode;
	var searchQuery = req.query.q || '';
	var paginationPage = req.params.page ? req.params.page : 1;

	var page = {
		page: thisPage,
		pages: {
			anime: front.config.pages.anime,
			episode: front.config.pages.episode
		},
		originalUrl: req.originalUrl,
		title: thisPage.title.sprintf(searchQuery),
		description: thisPage.description.sprintf(config.name),
		hasFooter: true,
		isAjax: req.query.ajax ? true : false,
		searchQuery: searchQuery,
		socialImage: utils.getDefaultSocialImage(),
		partners: utils.getPartners()
	};

	page.bodyClass = 'search-episode';

	page.styles = utils.getFrontStyles([]);

	if ( page.isAjax ) {
		page.scripts = utils.getFrontAjaxScripts([]);
	} else {
		page.scripts = utils.getFrontScripts([]);
	}

	page.topMenu = front.config.topMenu;

	page.breadcrumb = [
		{
			title: front.config.pages.index.title,
			url: front.config.pages.index.url
		},
		{
			title: page.title
		}
	];

	var objEpisodes = new Episode();
	var searchItemsPerPage = config.xBigList.perPage;
	objEpisodes.searchByTitlePaged(paginationPage, searchItemsPerPage, searchQuery, function(err, rEpisodes) {
		if (err) {
			//return utils.redirect(res, front.config.pages.notFound.url);
			return utils.notFound(req, res);
		}

		objEpisodes.countBySearch(searchQuery, function(err, length) {
			if (err) {
				//return utils.redirect(res, front.config.pages.notFound.url);
				return utils.notFound(req, res);
			}

			var lastPage = Math.ceil(length / searchItemsPerPage);
			var pagination = {
				page: parseInt(paginationPage),
				pageUrl: thisPage.urlPaged.sprintf('%s', searchQuery),
				lastPage: lastPage
			};

			var renderObject = {
				config: config,
				utils: utils,
				page: page,
				itemList: rEpisodes,
				pagination: pagination
			};

			res.render(thisPage.viewPath, renderObject);
		});

	});
}

exports.adAlternative = function(req, res) {
	var thisPage = front.config.pages.adAlternative;

	var page = {
		page: thisPage,
		originalUrl: req.originalUrl,
		title: thisPage.title,
		isHome: false,
		description: thisPage.description,
		hasFooter: true,
		isAjax: req.query.ajax ? true : false
	};
	page.bodyClass = '';

	page.styles = [
		{ href: "https://fonts.googleapis.com/css?family=Roboto:400,300,500", absolutUrl: true },
		{ href: 'front-ad-alternative.css' }
	];
	page.scripts = [];

	var frases = [
		[ "A vida é melhor para aqueles que fazem o possível para ter o melhor.", "John Wooden" ],
		[ "Os empreendedores falham, em média, 3,8 vezes antes do sucesso final. O que separa os bem-sucedidos dos outros é a persistência.", "Lisa M. Amos" ],
		[ "Se você não está disposto a arriscar, esteja disposto a uma vida comum.", "Jim Rohn" ],
		[ "Para de perseguir o dinheiro e comece a perseguir o sucesso.", "Tony Hsieh" ],
		[ "Todos os seus sonhos podem se tornar realidade se você tem coragem para persegui-los.", "Walt Disney" ],
		[ "Ter sucesso é falhar repetidamente, mas sem perder o entusiasmo.", "Winston Churchill" ],
		[ "Sempre que você vir uma pessoa de sucesso, você sempre verá as glórias, nunca os sacrifícios que os levaram até ali.", "Vaibhav Shah" ],
		[ "Oportunidades não surgem. É você que as cria.", "Chris Grosser" ],
		[ "Não tente ser uma pessoa de sucesso. Em vez disso, seja uma pessoa de valor.", "Albert Einstein" ],
		[ "Não é o mais forte que sobrevive, nem o mais inteligente. Quem sobrevive é o mais disposto à mudança.", "Charles Darwin" ],
		[ "A melhor vingança é um sucesso estrondoso.", "Frank Sinatra" ]
	];

	var count = frases.length;
	var randomI = Math.floor(Math.random() * count);

	var message = frases[randomI];

	res.render(thisPage.viewPath, {
		config: config,
		utils: utils,
		page: page,
		message: message
	});
}

exports.notFound = function(req, res) {
	res.header('Cache-Control', 'public, max-age=' + config.cache.maxAge);

	var thisPage = front.config.pages.notFound;

	var page = {
		page: thisPage,
		originalUrl: req.originalUrl,
		title: thisPage.title,
		isHome: false,
		description: thisPage.description,
		hasFooter: true,
		isAjax: req.query.ajax ? true : false,
		partners: utils.getPartners()
	};
	page.bodyClass = '';

	page.styles = utils.getFrontStyles([]);
	if ( page.isAjax ) {
		page.scripts = utils.getFrontAjaxScripts([]);
	} else {
		page.scripts = utils.getFrontScripts([]);
	}

	page.topMenu = front.config.topMenu;

	res.render(thisPage.viewPath, {
		config: config,
		utils: utils,
		page: page
	});
}

exports.sitemap = function(req, res) {
	//var thisPage = front.config.pages.sitemap;
	var sm = require('sitemap'),
		frequency = {
			hour: 'hourly',
			day: 'daily',
			week: 'weekly',
			month: 'monthly'
		},
		priority = {
			home: 1.0,
			veryHigh: 0.8,
			high: 0.6,
			medium: 0.5,
			low: 0.3
		};

	var sitemap = sm.createSitemap ({
		hostname: configEnv.url,
		cacheTime: 600000
	});

	var sitemapSimplePages = [
		front.config.pages.index,
		front.config.pages.animes
	];

	for( i in sitemapSimplePages ) {
		var sitemapSimplePage = sitemapSimplePages[i];
		sitemap.add({
			url: sitemapSimplePage.url,
			changefreq: sitemapSimplePage.changefreq,
			priority: sitemapSimplePage.priority
		});
	}

	var objSerial = new Serial();
	objSerial.getAll(function(err, rSerials) {
		if (err) {
			return utils.notFound(req, res);
		}

		var animePage = front.config.pages.anime;
		for(i in rSerials) {
			var anime = rSerials[i];
			sitemap.add({
				url: animePage.url.sprintf(anime.slug),
				changefreq: animePage.changefreq,
				priority: animePage.priority
			});
		}

		var objEpisodes = new Episode();
		objEpisodes.aggregateAllOrderedByTitle(function(err, rEpisodes) {
			if (err) {
				return utils.notFound(req, res);
			}

			var episodePage = front.config.pages.episode;
			for(j in rEpisodes) {
				var episode = rEpisodes[j];
				sitemap.add({
					url: episodePage.url.sprintf(episode._owner.slug, episode.slug),
					changefreq: episodePage.changefreq,
					priority: episodePage.priority
				});
			}

			sitemap.toXML( function (err, xml) {
				if (err) {
					return res.status(500).end();
				}
				res.header('Cache-Control', 'public, max-age=31557600')
				res.header('Content-Type', 'application/xml');
				res.send( xml );
			});
		})

	});
}

exports.notificationManifest = function(req, res) {
	res.header('Cache-Control', 'public, max-age=' + config.cache.maxAge);

	var thisPage = front.config.pages.notificationManifest;

	var page = {};

	//res.header('Cache-Control', 'public, max-age=31557600');
	res.header('Content-Type', 'application/json');

	res.render(thisPage.viewPath, {
		config: config,
		utils: utils,
		page: page
	});
}

exports.notificationWorker = function(req, res) {
	res.header('Cache-Control', 'public, max-age=' + config.cache.maxAge);
	
	var thisPage = front.config.pages.notificationWorker;

	var page = {};

	res.header('Content-Type', 'application/javascript');

	res.render(thisPage.viewPath, {
		config: config,
		utils: utils,
		page: page
	});
}