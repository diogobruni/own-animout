var ordinary = require('./ordinaryAjax'),
	multipart = require('./multipartAjax');

exports.init = function(req, res) {
	if ( ~req.headers['content-type'].indexOf('multipart/form-data') ) {
		multipart.init( req, res );
	} else {
		ordinary.init( req, res );
	}
}