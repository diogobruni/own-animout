var config = require("../config/config"),
	env = process.env.NODE_ENV || 'development',
	configEnv = config[env];
	front = {
		config: require("../config/front")
	},
	admin = {
		config: require("../config/admin")
	},
	utils = require('../lib/utils'),
	userControl = require('../lib/userControl');

var Mongoose = require('mongoose'),
	User = Mongoose.model('User'),
	Serial = Mongoose.model('Serial'),
	Episode = Mongoose.model('Episode');

exports.dashboard = function(req, res) {
	var sessionUser = req[configEnv.sessionConfig.cookieName].user;
	var thisPage = admin.config.pages.dashboard;

	userControl.isLoggedMinLevel(sessionUser, thisPage.requiredLevel.view, function(objReturn) {
		switch( objReturn.statusCode ) {
			case -1:
			case 0:
				utils.redirect(res, front.config.pages.login.url);
			break;

			case 2:
				utils.redirect(res, admin.config.pages.lackOfPermission.url);
			break;

			case 1:
			default:
				var page = {
					title: thisPage.title,
					description: thisPage.description,
					reqUrl: req.url
				};
				page.bodyClass = '';

				page.styles = utils.getAdminStyles([]);
				page.scripts = utils.getAdminScripts([]);

				page.sidebarMenu = admin.config.sidebarMenu;

				var renderObject = {
					user: objReturn.user,
					config: config,
					utils: utils,
					page: page
				};

				res.render(thisPage.viewPath, renderObject);
			break;
		}
	});
}

exports.lackOfPermission = function(req, res) {
	var sessionUser = req[configEnv.sessionConfig.cookieName].user;
	var thisPage = admin.config.pages.lackOfPermission;
	
	userControl.isLoggedMinLevel(sessionUser, thisPage.requiredLevel.view, function(objReturn) {
		switch( objReturn.statusCode ) {
			case -1:
			case 0:
				utils.redirect(res, front.config.pages.login.url);
			break;

			case 2:
				utils.redirect(res, admin.config.pages.lackOfPermission.url);
			break;

			case 1:
			default:
				var page = {
					title: thisPage.title,
					description: thisPage.description,
					reqUrl: req.url
				};
				page.bodyClass = '';

				page.styles = utils.getAdminStyles([]);
				page.scripts = utils.getAdminScripts([]);

				page.sidebarMenu = admin.config.sidebarMenu;

				var renderObject = {
					user: objReturn.user,
					config: config,
					utils: utils,
					page: page
				};

				res.render(thisPage.viewPath, renderObject);
			break;
		}
		
	});

}

exports.serialNewEdit = function(req, res) {
	var sessionUser = req[configEnv.sessionConfig.cookieName].user;
	var editingSlug = req.params.slug ? req.params.slug : false;
	var thisPage = false;

	if ( editingSlug ) {
		thisPage = admin.config.pages.serialEdit;
	} else {
		thisPage = admin.config.pages.serialNew;
	}

	userControl.isLoggedMinLevel(sessionUser, thisPage.requiredLevel.view, function(objReturn) {
		switch( objReturn.statusCode ) {
			case -1:
			case 0:
				utils.redirect(res, front.config.pages.login.url);
			break;

			case 2:
				utils.redirect(res, admin.config.pages.lackOfPermission.url);
			break;

			case 1:
			default:
				var page = {
					title: thisPage.title,
					description: thisPage.description,
					reqUrl: req.url
				};
				page.bodyClass = 'serial-new-edit';

				page.styles = utils.getAdminStyles([]);
				page.scripts = utils.getAdminScripts([
					{ src: 'admin-serial.js' }
				]);

				page.sidebarMenu = admin.config.sidebarMenu;

				var form = {
					ajaxAction: 'admin-serial-new-edit',
					redirectTo: admin.config.pages.serialList.url,
					isEditing: false,
					genres: admin.config.genres.sort()
				}

				var renderObject = {
					user: objReturn.user,
					config: config,
					utils: utils,
					form: form,
					page: page
				};

				if (editingSlug) {
					var objSerial = new Serial();
					objSerial.getBySlug(editingSlug, function(err, result) {
						if (result) {
							renderObject.form.isEditing = true;
							renderObject.form.item = result;
							res.render(thisPage.viewPath, renderObject);
						}
					});
				} else {
					res.render(thisPage.viewPath, renderObject);
				}
			break;
		}
	});
}

exports.serialList = function(req, res) {
	var sessionUser = req[configEnv.sessionConfig.cookieName].user;
	var thisPage = admin.config.pages.serialList;
	
	userControl.isLoggedMinLevel(sessionUser, thisPage.requiredLevel.view, function(objReturn) {
		switch( objReturn.statusCode ) {
			case -1:
			case 0:
				utils.redirect(res, front.config.pages.login.url);
			break;

			case 2:
				utils.redirect(res, admin.config.pages.lackOfPermission.url);
			break;

			case 1:
			default:
				var page = {
					title: thisPage.title,
					description: thisPage.description,
					reqUrl: req.url
				};
				page.bodyClass = 'serial-list';

				page.styles = utils.getAdminStyles([]);
				page.scripts = utils.getAdminScripts([
					{ src: 'admin-serial.js' }
				]);

				page.sidebarMenu = admin.config.sidebarMenu;

				page.links = {
					serialNew: admin.config.pages.serialNew
				}

				var list = {
					ajaxListAction: 'ajax-serial-list',
					ajaxDeleteAction: 'ajax-serial-delete'
				}

				var renderObject = {
					user: objReturn.user,
					config: config,
					utils: utils,
					page: page,
					list: list
				};

				res.render(thisPage.viewPath, renderObject);
			break;
		}
	});
}

exports.episodeNewEdit = function(req, res) {
	var animoutConfig = require('../config/animout');
	
	var sessionUser = req[configEnv.sessionConfig.cookieName].user;
	var editingSlug = req.params.slug ? req.params.slug : false;
	var thisPage = false;

	if ( editingSlug ) {
		thisPage = admin.config.pages.episodeEdit;
	} else {
		thisPage = admin.config.pages.episodeNew;
	}
	
	userControl.isLoggedMinLevel(sessionUser, thisPage.requiredLevel.view, function(objReturn) {
		switch( objReturn.statusCode ) {
			case -1:
			case 0:
				utils.redirect(res, front.config.pages.login.url);
			break;

			case 2:
				utils.redirect(res, admin.config.pages.lackOfPermission.url);
			break;

			case 1:
			default:
				var selectedOwner = req.query.anime ? req.query.anime : false;

				var page = {
					title: thisPage.title,
					description: thisPage.description,
					reqUrl: req.url
				};
				page.bodyClass = 'episode-new-edit';

				page.styles = utils.getAdminStyles([]);
				page.scripts = utils.getAdminScripts([
					{ src: 'admin-episode.js' }
				]);

				page.sidebarMenu = admin.config.sidebarMenu;

				var form = {
					ajaxAction: 'admin-episode-new-edit',
					redirectTo: admin.config.pages.serialList.url,
					isEditing: false,
					genres: admin.config.genres.sort(),
					types: animoutConfig.episode.types,
					selectedOwner: selectedOwner
				}

				var renderObject = {
					user: objReturn.user,
					config: config,
					utils: utils,
					form: form,
					page: page
				};

				if (editingSlug) {
					var objEpisode = new Episode();
					objEpisode.getBySlug(editingSlug, function(err, result) {
						if (result) {
							renderObject.form.isEditing = true;
							renderObject.form.item = result;

							var objSerial = new Serial();
							objSerial.getById(result._owner, function(err, rTitle) {
								renderObject.form.item._ownerName = rTitle.title;
								res.render(thisPage.viewPath, renderObject);
							});
						}
					});
				} else {
					if (selectedOwner) {
						var objSerial = new Serial();
						objSerial.getBySlug(selectedOwner, function(err, rOwner) {
							if (err) {

							}

							form.owner = rOwner;
							res.render(thisPage.viewPath, renderObject);
						});
					} else {
						res.render(thisPage.viewPath, renderObject);
					}
				}
			break;
		}
	});
}

exports.episodeNewAnitube = function(req, res) {
	var sessionUser = req[configEnv.sessionConfig.cookieName].user;
	var thisPage = admin.config.pages.episodeNewAnitube;
	
	userControl.isLoggedMinLevel(sessionUser, thisPage.requiredLevel.view, function(objReturn) {
		switch( objReturn.statusCode ) {
			case -1:
			case 0:
				utils.redirect(res, front.config.pages.login.url);
			break;

			case 2:
				utils.redirect(res, admin.config.pages.lackOfPermission.url);
			break;

			case 1:
			default:
				var selectedOwner = req.query.anime ? req.query.anime : false;
				
				var page = {
					title: thisPage.title,
					description: thisPage.description,
					reqUrl: req.url
				};
				page.bodyClass = 'episode-new-anitube';

				page.styles = utils.getAdminStyles([]);
				page.scripts = utils.getAdminScripts([
					{ src: 'admin-episode-anitube.js' }
				]);

				page.sidebarMenu = admin.config.sidebarMenu;

				var form = {
					ajaxAction: 'admin-episode-new-anitube',
					redirectTo: admin.config.pages.episodeList.url,
					selectedOwner: selectedOwner
				}

				var renderObject = {
					user: objReturn.user,
					config: config,
					utils: utils,
					form: form,
					page: page
				};

				if (selectedOwner) {
					var objSerial = new Serial();
					objSerial.getBySlug(selectedOwner, function(err, rOwner) {
						if (err) {}

						form.owner = rOwner;
						res.render(thisPage.viewPath, renderObject);
					});
				} else {
					res.render(thisPage.viewPath, renderObject);
				}
			break;
		}
	});
}

exports.episodeList = function(req, res) {
	var sessionUser = req[configEnv.sessionConfig.cookieName].user;
	var thisPage = admin.config.pages.episodeList;
	
	userControl.isLoggedMinLevel(sessionUser, thisPage.requiredLevel.view, function(objReturn) {
		switch( objReturn.statusCode ) {
			case -1:
			case 0:
				utils.redirect(res, front.config.pages.login.url);
			break;

			case 2:
				utils.redirect(res, admin.config.pages.lackOfPermission.url);
			break;

			case 1:
			default:
				var page = {
					title: thisPage.title,
					description: thisPage.description,
					reqUrl: req.url
				};
				page.bodyClass = 'episode-list';

				page.styles = utils.getAdminStyles([]);
				page.scripts = utils.getAdminScripts([
					{ src: 'admin-episode.js' }
				]);

				page.sidebarMenu = admin.config.sidebarMenu;

				page.links = {
					episodeNew: admin.config.pages.episodeNew
				}

				var list = {
					ajaxListAction: 'ajax-episode-list',
					ajaxDeleteAction: 'ajax-episode-delete'
				}

				var renderObject = {
					user: objReturn.user,
					config: config,
					utils: utils,
					page: page,
					list: list
				};

				res.render(thisPage.viewPath, renderObject);
			break;
		}
	});
}

exports.userNewEdit = function(req, res) {
	var sessionUser = req[configEnv.sessionConfig.cookieName].user;
	var editingEmail = req.params.email ? req.params.email : false;
	var thisPage = false;

	if ( editingEmail ) {
		thisPage = admin.config.pages.userEdit;
	} else {
		thisPage = admin.config.pages.userNew;
	}
	
	userControl.isLoggedMinLevel(sessionUser, thisPage.requiredLevel.view, function(objReturn) {
		switch( objReturn.statusCode ) {
			case -1:
			case 0:
				utils.redirect(res, front.config.pages.login.url);
			break;

			case 2:
				utils.redirect(res, admin.config.pages.lackOfPermission.url);
			break;

			case 1:
			default:
				var page = {
					title: thisPage.title,
					description: thisPage.description,
					reqUrl: req.url
				};
				page.bodyClass = 'user-new-edit';

				page.styles = utils.getAdminStyles([]);
				page.scripts = utils.getAdminScripts([
					{ src: 'admin-user.js' }
				]);

				page.sidebarMenu = admin.config.sidebarMenu;

				var form = {
					ajaxAction: 'admin-user-new-edit',
					redirectTo: admin.config.pages.userList.url,
					isEditing: false,
					levels: admin.config.levels
				}

				var renderObject = {
					user: objReturn.user,
					config: config,
					utils: utils,
					form: form,
					page: page
				};

				if (editingEmail) {
					var objUser = new User();
					objUser.getByEmail(editingEmail, function(err, result) {
						if (result) {
							renderObject.form.isEditing = true;
							renderObject.form.item = result;
							res.render(thisPage.viewPath, renderObject);
						}
					});
				} else {
					res.render(thisPage.viewPath, renderObject);
				}
			break;
		}
	});
}

exports.userList = function(req, res) {
	var sessionUser = req[configEnv.sessionConfig.cookieName].user;
	var thisPage = admin.config.pages.userList;
	
	userControl.isLoggedMinLevel(sessionUser, thisPage.requiredLevel.view, function(objReturn) {
		switch( objReturn.statusCode ) {
			case -1:
			case 0:
				utils.redirect(res, front.config.pages.login.url);
			break;

			case 2:
				utils.redirect(res, admin.config.pages.lackOfPermission.url);
			break;

			case 1:
			default:
				var page = {
					title: thisPage.title,
					description: thisPage.description,
					reqUrl: req.url
				};
				page.bodyClass = 'user-list';

				page.styles = utils.getAdminStyles([]);
				page.scripts = utils.getAdminScripts([
					{ src: 'admin-user.js' }
				]);

				page.sidebarMenu = admin.config.sidebarMenu;

				page.links = {
					userNew: admin.config.pages.userNew
				}

				var list = {
					ajaxListAction: 'ajax-user-list',
					ajaxDeleteAction: 'ajax-user-delete'
				}

				var renderObject = {
					user: objReturn.user,
					config: config,
					utils: utils,
					page: page,
					list: list
				};

				res.render(thisPage.viewPath, renderObject);
			break;
		}
	});
}