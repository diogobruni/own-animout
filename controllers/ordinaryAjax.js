var config = require( '../config/config'),
	env = process.env.NODE_ENV || 'development',
	configEnv = config[env],
	utils = require('../lib/utils'),
	animoutConfig = require('../config/animout'),
	admin = {
		config: require('../config/admin')
	},
	fileControl = require('../lib/fileControl'),
	anitube = require('../lib/anitube'),
	_ = require('lodash'),
	fs = require('fs'),
	async = require('async'),
	createTorrent = require('create-torrent');

var Mongoose = require('mongoose'),
	//Schema = Mongoose.Schema.
	User = Mongoose.model('User'),
	Serial = Mongoose.model('Serial'),
	Episode = Mongoose.model('Episode');


var ajaxActions = [];
exports.init = function(req, res) {

	/* Register Ajax Area */ // Example: registerAjax('ajaxSample', function(req, res) { });

	registerAjax('admin-login', function(req, res) {
		var email = req.body.email ? req.body.email : false;
		var password = req.body.password ? req.body.password : false;

		if (email && password) {
			var clientUser = new User({
				email: email,
				password: password
			});

			clientUser.authenticateUser(function(err, user) {
				if (user.length) {
					user = user[0];
					req[configEnv.sessionConfig.cookieName].user = {_id: user._id, email: user.email, nickname: user.nickname};
					var response = { status: true, message: 'Autenticado!' };
					res.send(JSON.stringify(response));
				} else {
					var response = { status: false, message: 'Usuário inválido!' };
					res.send(JSON.stringify(response));
				}
			});
		} else {
			var response = { status: false, message: 'Campos inválidos!' };
			res.send(JSON.stringify(response));
		}
	});

	registerAjax('ajax-serial-list', function(req, res) {
		if ( !utils.isAdminLoggedIn(req) ) {
			res.send('É necessário estar logado como admin para executar esse ajax.');
			return false;
		}

		var objSerial = new Serial();
		objSerial.getAllOrdererByLast(1, 99999, function( err, results ) {
			if ( !err ) {
				var dataResults = [];

				for ( i in results ) {
					var result = results[i];
					var admin = {
						config: require('../config/admin')
					};

					var actions = [],
						editUrl = admin.config.pages.serialEdit.url.replace( ':slug', result.slug ),
						addEpisodeUrl = admin.config.pages.episodeNew.url + '?anime=' + result.slug;

					actions.push('<a href="'+ addEpisodeUrl +'" class="">Adicionar Episódio</a>');
					actions.push('<a href="'+ editUrl +'" class="">Editar</a>');
					actions.push('<a href="javascript:void(0)" class="deleteItem">Excluir</a>');


					var coverHtml = '';
					if ( result.cover != '' ) {
						var coverUrl = utils.getCoverUrl( result.cover );
						coverHtml = '<img src="'+ coverUrl +'" class="img-responsive thumbnail"/>';
					}
					dataResults.push({
						DT_RowId: result._id,
						cover: coverHtml,
						title: result.title,
						episodes: '(coming soon)',
						actions: actions.join(' | ')
					});
				}

				var dataTableJson = { data: dataResults };
				res.send(JSON.stringify( dataTableJson ));
			} else {
				throw err;
			}
		});
	});

	registerAjax('ajax-serial-delete', function(req, res) {
		var sReturn = { status: true };

		var sessionUser = req[configEnv.sessionConfig.cookieName].user;
		var thisPage = admin.config.pages.serialList;
		userControl.isLoggedMinLevel(sessionUser, thisPage.requiredLevel.delete, function(objReturn) {
			switch( objReturn.statusCode ) {
				case -1:
				case 0:
					sReturn.status = false;
					sReturn.message = 'É necessário estar logado como admin para executar esse ajax.';
					res.send(JSON.stringify( sReturn ));
				break;

				case 2:
					sReturn.status = false;
					sReturn.message = 'Você não tem permissão para executar esta ação.';
					res.send(JSON.stringify( sReturn ));
				break;

				case 1:
				default:
					var where = { _id: req.body.id };
					Serial.findOne(where, function(err, result) {
						if (err) return handleError(err);

						if ( result ) {
							if ( result.cover ) {
								var pathFileToRemove = utils.getCoverDir() + result.cover;
								fs.access( pathFileToRemove, fs.F_OK, function() {
									fs.unlinkSync( pathFileToRemove );
								});
							}
							result.remove();
						} else {
							sReturn.status = false;
							sReturn.message = 'Não foi possível remover o usuário em questão.';
						}

						res.send(JSON.stringify( sReturn ));
					});
				break;
			}
		});
	});

	registerAjax('ajax-get-serials', function(req, res) {
		var searchQuery = req.body.q || '';

		var sReturn = {
			incomplete_results: false,
			results: [],
			total_count: 0
		};

		var objSerial = new Serial(),
			searchPage = 1,
			searchPerPage = config.xBigList.perPage;

		objSerial.searchByTitle(searchQuery, searchPage, searchPerPage, function(err, results) {
			//sReturn.results = results;
			for( i in results ) {
				var result = results[i];
				sReturn.results.push({
					id: result._id,
					name: result.title
				});
			}
			sReturn.total_count = results.length;
			res.send(JSON.stringify( sReturn ));
		});
	});

	registerAjax('ajax-get-ftp-files', function(req, res) {
		var searchQuery = req.body.q || '';

		var sReturn = {
			incomplete_results: false,
			results: [],
			total_count: 0
		};

		fs.readdir(config.ftpVideos, function(err, files) {
			if ( !err ) {
				for(i in files) {
					var file = files[i];
					sReturn.results.push({
						id: file,
						name: file
					});
				}
			}

			sReturn.total_count = sReturn.results.length;
			res.send(JSON.stringify( sReturn ));
		})
	});

	registerAjax('ajax-episode-list', function(req, res) {
		if ( !utils.isAdminLoggedIn(req) ) {
			res.send('É necessário estar logado como admin para executar esse ajax.');
			return false;
		}

		var objEpisode = new Episode();
		var where = {};
		var pSearch = req.body.search.value;
		if ( pSearch ) {
			where = {
				title: new RegExp(pSearch, 'i')
			};
		}
		var options = {
			limit: parseInt(req.body.length),
			skip: parseInt(req.body.start)
		};
		objEpisode.getCustom(where, options, function( err, results ) {
			if ( !err ) {
				var dataResults = [];

				for ( i in results ) {
					var result = results[i];
					var admin = {
						config: require('../config/admin')
					};

					var actions = [];
					var editUrl = admin.config.pages.episodeEdit.url.replace( ':slug', result.slug );

					actions.push('<a href="'+ editUrl +'" class="">Editar</a>');
					actions.push('<a href="javascript:void(0)" class="deleteItem">Excluir</a>');


					var videoLinkHtml = '<a href="'+ utils.getWebseedUrl(result.media.file) +'" target="_BLANK">'+ result.media.file +'</a>',
						torrentLinkHtml = '<a href="'+ utils.getTorrentUrl(result.media.torrent) +'" target="_BLANK">'+ result.media.torrent +'</a>';

					dataResults.push({
						DT_RowId: result._id,
						title: result.title,
						video: videoLinkHtml,
						torrent: torrentLinkHtml,
						actions: actions.join(' | ')
					});
				}

				objEpisode.count(function(err, totalCount) {
					if (err) return err;

					objEpisode.countBySearch(pSearch, function(err, searchCount) {
						if (err) return err;

						var dataTableJson = {
							data: dataResults,
							recordsFiltered: searchCount,
							recordsTotal: totalCount
						};
						res.send(JSON.stringify( dataTableJson ));
					});

				});

			} else {
				throw err;
			}
		});
	});

	registerAjax('ajax-episode-delete', function(req, res) {
		var sReturn = { status: true };

		var sessionUser = req[configEnv.sessionConfig.cookieName].user;
		var thisPage = admin.config.pages.episodeList;
		userControl.isLoggedMinLevel(sessionUser, thisPage.requiredLevel.delete, function(objReturn) {
			switch( objReturn.statusCode ) {
				case -1:
				case 0:
					sReturn.status = false;
					sReturn.message = 'É necessário estar logado como admin para executar esse ajax.';
					res.send(JSON.stringify( sReturn ));
				break;

				case 2:
					sReturn.status = false;
					sReturn.message = 'Você não tem permissão para executar esta ação.';
					res.send(JSON.stringify( sReturn ));
				break;

				case 1:
				default:
					var where = { _id: req.body.id };
					Episode.findOne(where, function(err, result) {
						if (err) return handleError(err);

						if ( result ) {
							if ( result.media.file ) {
								var fileToRemove = utils.getWebseedDir() + result.media.file;
								fs.access(fileToRemove, fs.F_OK, function(err) {
									if (err) { return false; }
									fs.unlink(fileToRemove, function() {});
								});
							}

							if ( result.media.torrent ) {
								var torrentToRemove = utils.getTorrentDir() + result.media.torrent;
								fs.access(torrentToRemove, fs.F_OK, function(err) {
									if (err) { return false; }
									fs.unlink(torrentToRemove, function() {});
								});
							}
							
							result.remove();
						} else {
							sReturn.status = false;
							sReturn.message = 'Não foi possível remover o episódio em questão.';
						}

						res.send(JSON.stringify( sReturn ));
					});
				break;
			}
		});
	});

	registerAjax('ajax-user-list', function(req, res) {
		if ( !utils.isAdminLoggedIn(req) ) {
			res.send('É necessário estar logado como admin para executar esse ajax.');
			return false;
		}

		var objUser = new User();
		objUser.getAllOrdererLevel( function( err, results ) {
			if ( !err ) {
				var dataResults = [];

				for ( i in results ) {
					var result = results[i];
					var admin = {
						config: require('../config/admin')
					};

					var actions = [],
						editUrl = admin.config.pages.userEdit.url.replace( ':email', result.email );

					actions.push('<a href="'+ editUrl +'" class="">Editar</a>');
					actions.push('<a href="javascript:void(0)" class="deleteItem">Excluir</a>');

					dataResults.push({
						DT_RowId: result._id,
						email: result.email,
						nickname: result.nickname,
						level: result.level,
						actions: actions.join(' | ')
					});
				}

				var dataTableJson = { data: dataResults };
				res.send(JSON.stringify( dataTableJson ));
			} else {
				throw err;
			}
		});
	});

	registerAjax('ajax-user-delete', function(req, res) {
		var sReturn = { status: true };

		var sessionUser = req[configEnv.sessionConfig.cookieName].user;
		var thisPage = admin.config.pages.userList;
		userControl.isLoggedMinLevel(sessionUser, thisPage.requiredLevel.delete, function(objReturn) {
			switch( objReturn.statusCode ) {
				case -1:
				case 0:
					sReturn.status = false;
					sReturn.message = 'É necessário estar logado como admin para executar esse ajax.';
					res.send(JSON.stringify( sReturn ));
				break;

				case 2:
					sReturn.status = false;
					sReturn.message = 'Você não tem permissão para executar esta ação.';
					res.send(JSON.stringify( sReturn ));
				break;

				case 1:
				default:
					var where = { _id: req.body.id };
					User.findOne(where, function(err, result) {
						if (err) return handleError(err);

						if ( result ) {
							result.remove();
						} else {
							sReturn.status = false;
							sReturn.message = 'Não foi possível remover o usuário em questão.';
						}

						res.send(JSON.stringify( sReturn ));
					});
				break;
			}
		});

	});

	registerAjax('admin-episode-new-anitube', function(req, res) {
		if ( !utils.isAdminLoggedIn(req) ) {
			res.send('É necessário estar logado como admin para executar esse ajax.');
			return false;
		}

		var sReturn = { status: true, errors: {
			failedUrls: []
		} };

		var ownerId = req.body._owner ? req.body._owner : false;
		var episodeUrlList = req.body.episodeUrlList ? req.body.episodeUrlList : false;

		if ( !ownerId ) {
			sReturn.status = false;
			sReturn.errors['_owner'] = 'Esse campo é obrigatório!';
		}

		if ( !episodeUrlList ) {
			sReturn.status = false;
			sReturn.errors['episodeUrlList'] = 'Esse campo é obrigatório!';
		}

		if ( sReturn.status ) {
			var episodesUrls = episodeUrlList.split("\r\n");
			var destination = utils.getWebseedDir();
			
			var eachEpisodeFunc = function(url, eachCallback) {
				anitube.download(url, destination, false, function(anitubeReturn) {
					if ( anitubeReturn.status ) {
						var opts = {
							urlList: [ utils.getWebseedUrl( anitubeReturn.fileName ) ]
						};

						createTorrent(anitubeReturn.filePath, opts, function(err, torrent) {
							if (err) {
								sReturn.errors['failedUrls'].push(url);
								fileControl.removeFile(anitubeReturn.filePath);
								console.log("Create Torrent Fail:", anitubeReturn.filePath);
								return eachCallback();
							} else {
								var torrentName = anitubeReturn.fileName + '.torrent';
								var torrentPath = utils.getTorrentDir() + torrentName;
								fs.writeFile(torrentPath, torrent, function() {
									var newEpisode = {
										_owner: ownerId,
										uploader: req[configEnv.sessionConfig.cookieName].user._id,
										title: anitubeReturn.title,
										slug: anitubeReturn.slug,
										media: {
											torrent: torrentName,
											file: anitubeReturn.fileName
										},
										type: {}
									};

									// Set type(normal|filer|ova|movie|teaser)
									var episodeTypes = animoutConfig.episode.types;
									var selectedType = 'normal';
									var matches = animoutConfig.episode.typeMatches;
									for( type in matches ) {
										for( key in matches[type] ) {
											var strKey = matches[type][key];
											if ( ~anitubeReturn.title.toLowerCase().indexOf( strKey ) ) {
												selectedType = type;
											}
										}
									}
									for( i in episodeTypes ) {
										var boolVal = false;
										if ( episodeTypes[i].slug == selectedType ) {
											boolVal = true;
										}
										newEpisode.type[episodeTypes[i].slug] = boolVal;
									}

									// Set episode number
									var regexEpisodeNumber = /(\d+(\.\d+)?)/;
									var matchEpisodeNumber = anitubeReturn.title.match(regexEpisodeNumber);
									if ( matchEpisodeNumber ) {
										newEpisode.number = parseInt(matchEpisodeNumber[0]);
									} else {
										newEpisode.number = 0;
									}
									

									var objEpisode = new Episode(newEpisode);
									objEpisode.insert(function() {
										return eachCallback();
									});
								});
							}
						});

					} else {
						sReturn.errors['failedUrls'].push(url);
						console.log("Download Fail:", url);
						return eachCallback();
					}
				});
			};

			var endOfLoopFunc = function(err) {
				res.send( JSON.stringify(sReturn) );
			}

			async.each( episodesUrls, eachEpisodeFunc, endOfLoopFunc);
		} else {
			res.send( JSON.stringify(sReturn) );
		}
		
	});
	
	/* /Register Ajax Area */

	if (req.body.action) {
		var action = req.body.action;
		if (ajaxActions[action] != undefined) {
			ajaxActions[action].function(req, res);
		} else {
			res.send('O ajax requisitado não existe ou não está disponível.');
		}
	}

}

function registerAjax(action, callback) {
	ajaxActions[action] = { action: action, function: callback};
}