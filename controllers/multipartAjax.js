var Mongoose = require('mongoose'),
	User = Mongoose.model('User'),
	Serial = Mongoose.model('Serial'),
	Episode = Mongoose.model('Episode'),

	config = require( '../config/config'),
	env = process.env.NODE_ENV || 'development',
	configEnv = config[env],

	animoutConfig = require('../config/animout'),

	utils = require('../lib/utils'),
	fileControl = require('../lib/fileControl'),
	encoder = require('../lib/encoder'),

	path = require('path'),
    //os = require('os'),
    //tempDir = os.tmpDir(),
    tempDir = config.tempDir,

    fs = require('fs'),
	Busboy = require('busboy'),
	createTorrent = require('create-torrent'),

	admin = {
		config: require('../config/admin')
	};

var ajaxActions = [];
exports.init = function(req, res) {

	registerAjax('admin-serial-new-edit', function(body, busboyCallback) {
		if ( !utils.isAdminLoggedIn(req) ) {
			res.send('É necessário estar logado para exercer essa função.');
			return false;
		}

		var sReturn = { status: true, errors: {} };

		var idEdit = typeof( body.id ) == 'string' ? body.id : false;

		var requiredFields = [
			'title'
		];
		
		var extraFields = [
			'genre',
			'synopsis'
		];

		if ( body['hasImage'] != 1 ) {
			extraFields.push( 'cover' );
		}

		var objFields = {};

		for( i in requiredFields ) {
			var field = requiredFields[i];

			if ( body[ field ] && body[ field ].trim() != '' ) {
				objFields[ field ] = body[ field ];
			} else {
				sReturn.status = false;
				sReturn.errors[ field ] = 'Esse campo é obrigatório.';
			}
		}

		for( i in extraFields ) {
			var field = extraFields[i];

			objFields[ field ] = body[ field ] ? body[ field ] : '';
		}

		if ( sReturn.status ) {
			var slugify = require("underscore.string/slugify");
			objFields.slug = slugify( objFields.title );

			if ( idEdit ) {
				objFields[ '_id' ] = idEdit;
			}

			var objSerial = new Serial( objFields );
			objSerial.checkIfExists( function( err, rCount ) {
				if ( rCount ) {
					sReturn.status = false;
					sReturn.errors['title'] = 'Este título já está cadastrado!';
					res.send( JSON.stringify(sReturn) );
					return busboyCallback();
				} else {

					var uploadStep = function(uploadCallback) {
						if ( objFields['cover'] ) {
							/* Cria nova imagem */
							var fileName = objFields.slug + path.extname( objFields['cover'] ),
								filePath = utils.getCoverDir() + fileName;

							var tempFile = objFields['cover'];
							objFields['cover'] = objSerial.cover = fileName;

							if ( idEdit ) {
								// Verifica se a versão anterior possuia imagem e a remove do sistema
								// caso o nome seja diferente do atual pois o upload já substitui a imagem anterior
								var tempObjSerial = new Serial();
								tempObjSerial.getById( idEdit, function( err, result ) {
									if (err) { return false; }
									if ( result && result.cover ) {
										if ( result.cover != objFields['cover'] ) {
											var fileToRemove = utils.getCoverDir() + result.cover;
											fs.access(fileToRemove, fs.F_OK, function(err) {
												if (err) { return false; }
												fs.unlink(fileToRemove, function() {});
											});
										}
									}
								});
							}

							fs.access(tempFile, fs.F_OK, function(err) {
								if (err) return uploadCallback();
								
								fs.rename(tempFile, filePath, function(err) {
									return uploadCallback();
								});
							});
							
						} else if ( idEdit && body['hasImage'] == 0 ) {
							var tempObjSerial = new Serial();
							tempObjSerial.getById( idEdit, function( err, result ) {
								if (err) { return uploadCallback(); }

								if (result.cover != '') {
									var fileToRemove = utils.getCoverDir() + result.cover;
									fs.access( fileToRemove, fs.F_OK, function(err) {
										if (err) { return uploadCallback(); }
										fs.unlink(fileToRemove, function() {
											return uploadCallback();
										});
									});
								} else {
									return uploadCallback();
								}
							});
						} else {
							return uploadCallback();
						}
					}

					uploadStep(function() {
						if ( idEdit ) {
							objSerial.update( function( createdRegistry ) {
								sReturn.redirectTo = admin.config.pages.serialEdit.url.replace( ':slug', objSerial.slug );
								res.send( JSON.stringify(sReturn) );
								return busboyCallback();
							});
						} else {
							objSerial.insert( function( createdRegistry ) {
								sReturn.redirectTo = admin.config.pages.serialEdit.url.replace( ':slug', objSerial.slug );
								res.send( JSON.stringify(sReturn) );
								return busboyCallback();
							});
						}
					});
				}
			});
		} else {
			res.send( JSON.stringify(sReturn) );
			return busboyCallback();
		}
	});

	registerAjax('admin-episode-new-edit', function(body, busboyCallback) {
		if ( !utils.isAdminLoggedIn(req) ) {
			res.send('É necessário estar logado para exercer essa função.');
			return false;
		}

		var sReturn = { status: true, errors: {} };

		var idEdit = typeof( body.id ) == 'string' ? body.id : false;

		var requiredFields = [
			'_owner',
			'title',
			'itemType',
			'number'
		];

		/*
		if ( body['hasMedia'] != 1 ) {
			requiredFields.push('media');
		}
		*/
		
		var extraFields = [
			'release',
			'media',
			'hotlinkUrl',
			'ftpFile',
			'youtubeUrl',
			'tags',
			'description'
		];

		var episodeTypes = animoutConfig.episode.types;

		var objFields = {};

		for( i in requiredFields ) {
			var field = requiredFields[i];

			if ( body[ field ] && body[ field ].trim() != '' ) {
				objFields[ field ] = body[ field ];
			} else {
				sReturn.status = false;
				sReturn.errors[ field ] = 'Esse campo é obrigatório.';
			}
		}

		for( i in extraFields ) {
			var field = extraFields[i];
			objFields[ field ] = body[ field ] ? body[ field ] : '';
		}

		/* Custom Verification */
		if ( body['hasMedia'] != 1 ) {
			if ( !objFields['media'] && !objFields['hotlinkUrl'] && !objFields['ftpFile'] && !objFields['youtubeUrl'] ) {
				sReturn.status = false;
				sReturn.errors['media'] = sReturn.errors['hotlinkUrl'] = sReturn.errors['ftpFile'] = 'Preencha pelo menos uma forma de upload.';
			}
		}

		// Set type(normal|filer|ova|movie|teaser)
		objFields['type'] = {};
		for( i in episodeTypes ) {
			var episodeType = episodeTypes[i].slug;
			var boolValue = false;
			if ( episodeType == objFields['itemType'] ) {
				boolValue = true;
			}
			objFields['type'][episodeType] = boolValue;
		}

		if ( idEdit ) {
			objFields[ '_id' ] = idEdit;
		}

		if ( sReturn.status ) {
			var slugify = require("underscore.string/slugify");
			objFields.slug = slugify( objFields.title );

			var objEpisode = new Episode( objFields );
			objEpisode.checkIfExists( function( err, rCount ) {
				if ( rCount ) {
					sReturn.status = false;
					sReturn.errors['title'] = 'Este episódio já está cadastrado!';
					res.send( JSON.stringify(sReturn) );
					return busboyCallback();
				} else {
					var uploadStep = function(uploadCallback) {
						var uploadReturn = {
							status: true,
							errors: []
						};

						if ( objFields['media'] ) {
							var fileExt = path.extname( objFields['media'] ),

								fileName = objFields.slug + fileExt,
								encodedFileName = objFields.slug + animoutConfig.episode.file.defaultExtension,

								torrentName = objFields.slug + path.extname( objFields['media'] ) + '.torrent',
								encodedTorrentName = objFields.slug + animoutConfig.episode.file.defaultExtension + '.torrent',

								filePath = utils.getWebseedDir() + fileName,
								preEncoderFilePath = utils.getPreEncoderDir() + fileName,
								encodedFilePath = utils.getWebseedDir() + encodedFileName,

								torrentPath = utils.getTorrentDir() + torrentName,
								encodedTorrentPath = utils.getTorrentDir() + encodedTorrentName,

								tempFile = objFields['media'];

							objEpisode.media.file = fileName;
							objEpisode.media.torrent = torrentName;

							if ( idEdit ) {
								// Verifica se a versão anterior possuia imagem e a remove do sistema
								// caso o nome seja diferente do atual pois o upload já substitui a imagem anterior
								var tempObjEpisode = new Episode();
								tempObjEpisode.getById( idEdit, function( err, result ) {
									if (err) { return false; }
									if ( result && fileName ) {
										if ( result.media.file != fileName ) {
											var fileToRemove = utils.getWebseedDir() + result.media.file;
											fileControl.removeFile(fileToRemove, function(status) {});
										}

										if ( result.media.torrent != torrentName ) {
											var torrentToRemove = utils.getTorrentDir() + result.media.torrent;
											fileControl.removeFile(torrentToRemove, function(status) {});
										}
									}
								});
							}

							if ( env == 'development' ) {
								fileControl.renameFile(tempFile, filePath, function(status) {
									if (!status) {
										uploadReturn.status = false;
										uploadReturn.errors['media'] = 'Erro no upload do arquivo, tente novamente.';
										return uploadCallback(uploadReturn);
									}

									var opts = {
										urlList: [ utils.getWebseedUrl(fileName) ]
									};

									createTorrent(filePath, opts, function (err, torrent) {
										if (err) {
											uploadReturn.status = false;
											uploadReturn.errors['media'] = 'Erro na criação do torrent do arquivo de upload, tente novamente.';
											fileControl.removeFile(filePath, function(status) {
												return uploadCallback(uploadReturn);
											});
										} else {
											fs.writeFile(torrentPath, torrent, function() {
												return uploadCallback(uploadReturn);
											});
										}
									});
								});
							} else {
								filePath = preEncoderFilePath;
								fileControl.renameFile(tempFile, filePath, function(status) {
									if (!status) {
										uploadReturn.status = false;
										uploadReturn.errors['media'] = 'Erro no upload do arquivo, tente novamente.';
										return uploadCallback(uploadReturn);
									}

									tempFile = filePath;

									fileName = encodedFileName;
									filePath = encodedFilePath;
									torrentName = encodedTorrentName;
									torrentPath = encodedTorrentPath;

									objEpisode.media.file = fileName;
									objEpisode.media.torrent = torrentName;

									encoder.encode(tempFile, filePath, function(err, output) {
										if (err) {
											uploadReturn.status = false;
											uploadReturn.errors['media'] = 'Erro no upload do arquivo, tente novamente.';
											return uploadCallback(uploadReturn);
										}

										fileControl.removeFile(tempFile, function(status) {});

										var opts = {
											urlList: [ utils.getWebseedUrl(fileName) ]
										};

										createTorrent(filePath, opts, function (err, torrent) {
											if (err) {
												uploadReturn.status = false;
												uploadReturn.errors['media'] = 'Erro na criação do torrent do arquivo de upload, tente novamente.';
												fileControl.removeFile(filePath, function(status) {
													return uploadCallback(uploadReturn);
												});
											} else {
												fs.writeFile(torrentPath, torrent, function() {
													return uploadCallback(uploadReturn);
												});
											}
										});
									});
								});
							}
						} else if ( objFields['ftpFile'] ) {
							var ftpFileName = objFields['ftpFile'],
								ftpFileExt = path.extname(ftpFileName),
								ftpFilePath = config.ftpVideos + ftpFileName,

								fileName = objFields.slug + ftpFileExt,
								filePath = utils.getWebseedDir() + fileName;

								torrentName = fileName + '.torrent',
								torrentPath = utils.getTorrentDir() + torrentName,

								encodedFileName = objFields.slug + animoutConfig.episode.file.defaultExtension,
								encodedTorrentName = encodedFileName + '.torrent',
								encodedTorrentPath = utils.getTorrentDir() + encodedTorrentName,

								preEncoderFilePath = utils.getPreEncoderDir() + fileName,
								encodedFilePath = utils.getWebseedDir() + encodedFileName;


							if ( env == 'development' ) {
								objEpisode.media.file = fileName;
								objEpisode.media.torrent = torrentName;

								fileControl.renameFile(ftpFilePath, filePath, function(status) {
									if (!status) {
										uploadReturn.status = false;
										uploadReturn.errors['media'] = 'Erro no upload do arquivo, tente novamente.';
										return uploadCallback(uploadReturn);
									}

									var opts = {
										urlList: [ utils.getWebseedUrl(fileName) ]
									};

									createTorrent(filePath, opts, function (err, torrent) {
										if (err) {
											uploadReturn.status = false;
											uploadReturn.errors['media'] = 'Erro na criação do torrent do arquivo de upload, tente novamente.';
											fileControl.removeFile(filePath, function(status) {
												return uploadCallback(uploadReturn);
											});
										} else {
											fs.writeFile(torrentPath, torrent, function() {
												return uploadCallback(uploadReturn);
											});
										}
									});
								});
							} else {
								fileControl.renameFile(ftpFilePath, preEncoderFilePath, function(status) {
									if (!status) {
										uploadReturn.status = false;
										uploadReturn.errors['media'] = 'Erro no upload do arquivo, tente novamente.';
										return uploadCallback(uploadReturn);
									}

									objEpisode.media.file = encodedFileName;
									objEpisode.media.torrent = encodedTorrentName;

									encoder.encode(preEncoderFilePath, encodedFilePath, function(err, output) {
										if (err) {
											uploadReturn.status = false;
											uploadReturn.errors['media'] = 'Erro no upload do arquivo, tente novamente.';
											return uploadCallback(uploadReturn);
										}

										fileControl.removeFile(preEncoderFilePath, function(status) {});

										var opts = {
											urlList: [ utils.getWebseedUrl(encodedFileName) ]
										};

										createTorrent(encodedFilePath, opts, function (err, torrent) {
											if (err) {
												uploadReturn.status = false;
												uploadReturn.errors['media'] = 'Erro na criação do torrent do arquivo de upload, tente novamente.';
												fileControl.removeFile(encodedFilePath, function(status) {
													return uploadCallback(uploadReturn);
												});
											} else {
												fs.writeFile(encodedTorrentPath, torrent, function() {
													return uploadCallback(uploadReturn);
												});
											}
										});
									});
								});
							}

						} else if ( objFields['hotlinkUrl'] ) {
							var fileExt = '.mp4',
								fileName = objFields.slug + fileExt,
								torrentName = objFields.slug + fileExt + '.torrent',
								destination = utils.getWebseedDir(),
								filePath = destination + fileName,
								torrentPath = utils.getTorrentDir() + torrentName;

							objEpisode.media.file = fileName;
							objEpisode.media.torrent = torrentName;

							fileControl.removeFile(filePath, function() {

								var fileToDownload = objFields['hotlinkUrl'];
								fileControl.download(fileToDownload, filePath, function(cbReturn) {
									if ( !cbReturn.status ) {
										uploadReturn.status = false;
										uploadReturn.errors['hotlinkUrl'] = cbReturn.message;
										return uploadCallback(uploadReturn);
									}

									var opts = {
										urlList: [ utils.getWebseedUrl(fileName) ]
									};

									createTorrent(filePath, opts, function (err, torrent) {
										if (err) {
											uploadReturn.status = false;
											uploadReturn.errors['hotlinkUrl'] = 'Erro na criação do torrent do arquivo de upload, tente novamente.';
											fileControl.removeFile(filePath, function(status) {
												return uploadCallback(uploadReturn);
											});
										} else {
											fs.writeFile(torrentPath, torrent, function() {
												return uploadCallback(uploadReturn);
											});
										}
									});
								});
							});
						} else if ( objFields['youtubeUrl'] ) {
							// YoutubeUrl não necessita de upload
							objEpisode.youtube = objFields.youtubeUrl;
							return uploadCallback(uploadReturn);
						} else if ( idEdit && body['hasMedia'] == 0 ) {
							var tempObjEpisode = new Episode();
							tempObjEpisode.getById( idEdit, function( err, result ) {
								if (err) { return uploadCallback(uploadReturn); }

								if (result.media.file != '') {
									var fileToRemove = utils.getWebseedDir() + result.media.file;
									fileControl.removeFile(fileToRemove, function(status) {
										if (!status) {
											uploadReturn.status = false;
											uploadReturn.errors['media'] = 'Não foi possível remover o arquivo, tente novamente.';
											return uploadCallback(uploadReturn);
										}
										return uploadCallback(uploadReturn);
									})
								} else {
									uploadReturn.status = false;
									uploadReturn.errors['media'] = 'Erro, atualize a página e tente novamente.';
									return uploadCallback(uploadReturn);
								}
							});
						} else if ( idEdit && body['hasMedia'] == 1 ) {
							// Edit sem enviar novo arquivo
							return uploadCallback(uploadReturn);
						} else {
							uploadReturn.status = false;
							uploadReturn.errors['media'] = 'Erro, atualize a página e tente novamente.';
							return uploadCallback(uploadReturn);
						}
					}

					uploadStep(function(returnObj) {
						if ( returnObj.status ) {
							objEpisode.uploader = req[configEnv.sessionConfig.cookieName].user._id;
							if ( idEdit ) {
								objEpisode.update( function( createdRegistry ) {
									sReturn.redirectTo = admin.config.pages.episodeEdit.url.replace( ':slug', objEpisode.slug );
									res.send( JSON.stringify(sReturn) );
									return busboyCallback();
								});
							} else {
								objEpisode.insert( function( createdRegistry ) {
									sReturn.redirectTo = admin.config.pages.episodeEdit.url.replace( ':slug', objEpisode.slug );

									if ( env == 'development' ) {
										res.send( JSON.stringify(sReturn) );
										return busboyCallback();
									} else {
										var objSerial = new Serial();
										objSerial.getById( objEpisode._owner, function(err, rSerial) {
											if ( !err ) {
												var front = require('../config/front');
												var data = {
													title: animoutConfig.episode.notification.title.sprintf(objEpisode.title),
													message: animoutConfig.episode.notification.message.sprintf(objEpisode.title),
													url: configEnv.url + front.pages.episode.url.sprintf(rSerial.slug, objEpisode.slug)
												};
												var pushNote = require('../lib/pushNote');
												pushNote.sendNotification(data);
											}

											res.send( JSON.stringify(sReturn) );
											return busboyCallback();
										});
									}
								});
							}
						} else {
							sReturn.status = false;
							sReturn.errors = returnObj.errors;
							res.send( JSON.stringify(sReturn) );
							return busboyCallback();
						}
					});
				}
			});
		} else {
			res.send( JSON.stringify(sReturn) );
			return busboyCallback();
		}
	});

	registerAjax('admin-user-new-edit', function(body, busboyCallback) {
		if ( !utils.isAdminLoggedIn(req) ) {
			res.send('É necessário estar logado para exercer essa função.');
			return false;
		}

		var sReturn = { status: true, errors: {} };

		var idEdit = typeof( body.id ) == 'string' ? body.id : false;

		var requiredFields = [
			'email',
			'nickname',
			'level'
		];
		
		var extraFields = [
			'description'
		];

		if ( idEdit ) {
			extraFields.push('password');
		} else {
			requiredFields.push('password');
		}

		var objFields = {};

		for( i in requiredFields ) {
			var field = requiredFields[i];

			if ( body[ field ] && body[ field ].trim() != '' ) {
				objFields[ field ] = body[ field ];
			} else {
				sReturn.status = false;
				sReturn.errors[ field ] = 'Esse campo é obrigatório.';
			}
		}

		for( i in extraFields ) {
			var field = extraFields[i];

			objFields[ field ] = body[ field ] ? body[ field ] : '';
		}

		if ( sReturn.status ) {
			if ( idEdit ) {
				objFields[ '_id' ] = idEdit;
			}

			if ( idEdit && objFields['password'] == '' ) {
				delete objFields['password'];
			}
			var objUser = new User( objFields );
			objUser.checkExistEmail( function( err, boolUserExist ) {
				if ( boolUserExist ) {
					sReturn.status = false;
					sReturn.errors['email'] = 'Já existe um usuário com este e-mail!';
					res.send( JSON.stringify(sReturn) );
					return busboyCallback();
				} else {
					if ( idEdit ) {
						objUser.update( function( returnStatus ) {
							sReturn.redirectTo = admin.config.pages.userEdit.url.replace( ':email', objUser.email );
							res.send( JSON.stringify(sReturn) );
							return busboyCallback();
						});
					} else {
						objUser.insert( function( returnStatus ) {
							sReturn.redirectTo = admin.config.pages.userEdit.url.replace( ':email', objUser.email );
							res.send( JSON.stringify(sReturn) );
							return busboyCallback();
						});
					}
				}
			});
		} else {
			res.send( JSON.stringify(sReturn) );
			return busboyCallback();
		}
	});

	/* Make body request content */
	var tempFiles = [];
	var body = {};

	var busboy = new Busboy({ headers: req.headers });
    busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
    	if ( filename ) {
			var saveTo = path.join(tempDir, filename);
			file.pipe(fs.createWriteStream(saveTo));
			body[ fieldname ] = saveTo;
			tempFiles.push( saveTo );
    	} else {
    		file.on( 'data', function() {});
    		file.on( 'end', function() {});
    	}
    });

    busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated) {
    	if ( body[ fieldname ] ) {
    		if ( typeof( body[ fieldname ] ) == 'string' ) {
    			body[ fieldname ] = [ body[ fieldname ] ];
    		}
    		body[ fieldname ].push( val );
    	} else {
			body[ fieldname ] = val;
    	}
    });

	busboy.on('finish', function() {
		if ( body.action ) {
			var action = body.action;
			if (ajaxActions[action] != undefined) {
				ajaxActions[action].function( body, busboyCallback);
			} else {
				res.send('O ajax requisitado não existe ou não está disponível.');
				busboyCallback();
			}
		}

		function busboyCallback() {
			// Remove temp files
			for( i in tempFiles ) {
				fileControl.removeFile(tempFiles[i], function(status) {});
			}
		}
    });
    req.pipe(busboy);

}

function registerAjax(action, callback) {
	ajaxActions[action] = { action: action, function: callback};
}