var config = require('../config/config');

exports.sendNotification = function(objData) {
	var data = {
		app_id: config.notifications.one_signal.app_id,
		headings: {
			"en": objData.title,
			"br": objData.title
		},
		contents: {
			"en": objData.message,
			"br": objData.message,
		},
		url: objData.url,
		included_segments: ["All"]
	};

	var headers = {
		"Content-Type": "application/json",
		"Authorization": "Basic " + config.notifications.one_signal.rest_key
	};

	var options = {
		host: "onesignal.com",
		port: 443,
		path: "/api/v1/notifications",
		method: "POST",
		headers: headers
	};

	var https = require('https');
	var req = https.request(options, function(res) {  
		res.on('data', function(data) {
			//console.log("Response:");
			//console.log(JSON.parse(data));
		});
	});

	req.on('error', function(e) {
		//console.log("ERROR:");
		//console.log(e);
	});

	req.write(JSON.stringify(data));
	req.end();
}