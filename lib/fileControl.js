var fs = require('fs'),
	needle = require('needle');

exports.removeFile = function(file, callback) {
	var status = true;
	fs.access(file, fs.F_OK, function(err) {
		if (err) {
			status = false;
			return callback(status);
		}
		fs.unlink(file, function() {
			return callback(status);
		});
	});
}

exports.renameFile = function(oldFile, newFile, callback) {
	var status = true;
	fs.access(oldFile, fs.F_OK, function(err) {
		if (err) {
			status = false;
			return callback(status);
		}
		fs.rename(oldFile, newFile, function() {
			return callback(status);
		});
	});
}

exports.download = function(url, destination, callback) {
	var file = fs.createWriteStream(destination),
		options = {
			follow_max: 5
		},
		finished = false,
		callbackReturn = { status: true };

	var fileControl = this;
	
	needle.get(url, options).pipe(file);

	file.on('error', function() {
		file.close(function() {
			fileControl.removeFile(destination, function() {
				callbackReturn = {
					status: false,
					message: 'Erro no download'
				};
				callback(callbackReturn)
			});
		});
	});

	file.on('finish', function() {
		finished = true;
		file.close(function() {
			callback(callbackReturn);
		});
	});

	setTimeout(function() {
		if ( !finished ) {
			file.close(function() {
				fileControl.removeFile(destination, function() {
					callbackReturn = {
						status: false,
						message: 'Erro de timeout no download'
					};
					callback(callbackReturn)
				});
			});
		}
	}, 1000 * 60 * 10);
}