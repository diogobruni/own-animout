var config = require('../config/config'),
	utils = require('./utils'),
	fileControl = require('./fileControl'),
	needle = require('needle'),
	http = require('http'),
	validUrl = require('valid-url'),
	fs = require('fs'),
	path = require('path');

var variables = {
	//regexPage: /(http:\/\/anitubebr.xpg.uol.com.br\/player\/config\.php\?key=[a-zA-Z0-9]*)/,
	//regexPage: /(http|https):\/\/([\w.?=&\/]+)(config).+?([\w.?=&\/]+)/g, // By Henrique
	regexTitle: /<title>(.*)<\/title>/,
	regexPage: /(http|https)(:\/\/)([\w.?=&\/]+)(config.+?[\w.?=&\/]+)/g, // Pequena alteração
	regexConfig: /http.*\.mp4/g,
	indexQuality: '_hd.mp4'
};

function downloadFileTo(url, destination, downloadCallback) {
	var file = fs.createWriteStream(destination),
		options = {
			follow_max: 5
		},
		finished = false;
	
	needle.get(url, options).pipe(file);

	file.on('error', function() {
		file.close(function() {
			fileControl.removeFile(destination, function() {
				downloadFileTo(url, destination, downloadCallback);
			});
		});
	});

	file.on('finish', function() {
		finished = true;
		file.close(downloadCallback);
	});

	setTimeout(function() {
		if ( !finished ) {
			file.close(function() {
				fileControl.removeFile(destination, function() {
					downloadFileTo(url, destination, downloadCallback);
				});
			});
		}
	}, 60 * 1000);
}

exports.download = function(anitubeUrl, destination, fileName, anitubeCallback) {
	var sReturn = {
			status: true,
			video: {
				sd: {
					url: ''
				},
				hd: {
					url: ''
				}
			},
			message: '',
			fileName: '',
			filePath: '',
			title: '',
			slug: ''
		};
	needle.defaults({
		open_timeout: 60000,
		user_agent: 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36',
		follow_max: 5
	});

	if ( validUrl.isUri(anitubeUrl) ) {
		needle.get(anitubeUrl, function(errPage, respPage) {
			if (errPage) {
				sReturn.status = false;
				sReturn.message = 'Erro no request da url "'+ anitubeUrl +'"!';
				return anitubeCallback(sReturn);
			}

			var resultPage = respPage.body.match(variables.regexPage);
			if ( !resultPage || resultPage.length == 0 ) {
				sReturn.status = false;
				sReturn.message = 'Arquivo config não encontrado.';
				return anitubeCallback(sReturn);
			}
			
			var configUrl = resultPage.join();

			if ( !fileName ) {
				var auxPageTitle = respPage.body.match(variables.regexTitle);
				auxPageTitle = auxPageTitle[1];
				auxPageTitle = auxPageTitle.split(' - ');
				delete auxPageTitle[auxPageTitle.length-1];
				var pageTitle = auxPageTitle.join(' - ');
				pageTitle = pageTitle.substr(0, pageTitle.length - 3);
				sReturn.title = pageTitle;
				
				var slugify = require("underscore.string/slugify");
				var titleSlug = slugify(pageTitle);
				sReturn.slug = titleSlug;
				fileName = titleSlug + '.mp4';
			}

			if ( validUrl.isUri(configUrl) ) {
				var options = {
					compressed: true,
					follow_max: 5
				};

				var configStream = needle.get(configUrl, options),
					videoUrls,
					configContent;

				configStream.on('readable', function() {
					var data;
					while(data=this.read()) {
						configContent += data.toString();
					}
				})
				.on('end', function() {
					var videoUrls;
					videoUrls = configContent.match(variables.regexConfig);

					for( i in videoUrls ) {
						var currentVideoUrl = videoUrls[i];
						if ( ~currentVideoUrl.indexOf(variables.indexQuality) ) {
							sReturn.video.hd.url = currentVideoUrl;
						} else {
							sReturn.video.sd.url = currentVideoUrl;
						}
					}

					var downloadThis;

					if ( sReturn.video.hd.url ) {
						downloadThis = sReturn.video.hd.url;
					} else if ( sReturn.video.sd.url ) {
						downloadThis = sReturn.video.sd.url;
					}

					if ( validUrl.isUri(downloadThis) ) {
						var filePath = destination + fileName;
						sReturn.fileName = fileName;
						sReturn.filePath = filePath;

						fs.access(filePath, fs.F_OK, function(err) {
							if (err) {
								downloadFileTo(downloadThis, filePath, function() {
									return anitubeCallback(sReturn);
								});
							} else {
								sReturn.status = false;
								sReturn.message = 'O arquivo de vídeo "'+ fileName +'" já existe!';
								return anitubeCallback(sReturn);
							}
						});
					} else {
						sReturn.status = false;
						sReturn.message = 'Url de download inválida! ('+ downloadThis +')';
						return anitubeCallback(sReturn);
					}

				});
			} else {
				sReturn.status = false;
				sReturn.message = 'Url do arquivo de configuração do AniTube inválida! ('+ configUrl +')';
				return anitubeCallback(sReturn);
			}
		});
	} else {
		sReturn.status = false;
		sReturn.message = 'Url do AniTube inválida! ('+ anitubeUrl +')';
		return anitubeCallback(sReturn);
	}
}