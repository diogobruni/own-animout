var path = require('path'),
	_ = require('lodash'),
	config = require('../config/config'),
	env = process.env.NODE_ENV || 'development',
	configEnv = config[env],
	front = require('../config/front'),
	admin = require('../config/admin'),
	partners = require('../config/partners');

exports.s = require('underscore.string');
exports.moment = require('moment');

String.prototype.sprintf = String.prototype.template || function() {
	var args = Array.prototype.slice.call(arguments),
		str = this,
        i = 0;

    return str.replace(/%s/g, function() {
	    return args[i++];
	});
};

exports.redirect = function(res, location) {
	res.writeHead(302, {
		'Location': location
	});
	res.end();
}

exports.notFound = function(req, res) {
	var frontController = require('../controllers/front');
	frontController.notFound(req, res);
}

exports.isAdminLoggedIn = function(req, redir, res) {
	redir = redir ? redir : false;
	if (req[configEnv.sessionConfig.cookieName].user != undefined && req[configEnv.sessionConfig.cookieName].user.email != undefined) {
		return true;
	} else {
		if (redir && res) {
			req[configEnv.sessionConfig.cookieName].redirectAfterLogin = req.url;
			this.redirect(res, redir);
		}
		return false;
	}
}
exports.getAdminStyles = function(extraStyles, boolImportDefault) {
	var unifiedList = [];

	if (typeof(boolImportDefault) == 'undefined') {
		boolImportDefault = true;
	}

	if (boolImportDefault) {
		unifiedList = _.union(admin.dependencyStyles, admin.defaultStyles, extraStyles);
	} else {
		unifiedList = _.union(admin.dependencyStyles, extraStyles);
	}
	return unifiedList;
}
exports.getAdminScripts = function(extraScripts, boolImportDefault) {
	var unifiedList = [];

	if (typeof(boolImportDefault) == 'undefined') {
		boolImportDefault = true;
	}

	if (boolImportDefault) {
		unifiedList = _.union(admin.dependencyScripts, admin.defaultScripts, extraScripts);
	} else {
		unifiedList = _.union(admin.dependencyScripts, extraScripts);
	}
	return unifiedList;
}
exports.getFrontStyles = function(extraStyles, boolImportDefault) {
	var unifiedList = [];

	if (typeof(boolImportDefault) == 'undefined') {
		boolImportDefault = true;
	}

	if (boolImportDefault) {
		unifiedList = _.union(front.dependencyStyles, front.defaultStyles, extraStyles);
	} else {
		unifiedList = _.union(front.dependencyStyles, extraStyles);
	}
	return unifiedList;
}
exports.getFrontScripts = function(extraScripts, boolImportDefault) {
	var unifiedList = [];

	if (typeof(boolImportDefault) == 'undefined') {
		boolImportDefault = true;
	}

	if (boolImportDefault) {
		unifiedList = _.union(front.dependencyScripts, front.defaultScripts, extraScripts);
	} else {
		unifiedList = _.union(front.dependencyScripts, extraScripts);
	}
	return unifiedList;
}
exports.getFrontAjaxScripts = function(extraScripts) {
	return _.union( front.ajaxScripts, extraScripts );
}
exports.jsUrl = function(obj) {
	var returnUrl = '';
	if ( obj.absolutUrl == true ) {
		returnUrl = obj.src;
	} else {
		returnUrl = '/' + config.jsDir + "/" + obj.src;
	}

	return returnUrl;
}
exports.jsScript = function(obj) {
	var url = utils.jsUrl(obj),
		id = typeof(obj.id) != 'undefined' ? ' id="obj.id"' : '',
		async = typeof(obj.async) != 'undefined' ? ' async' : '';

	return '<script src="'+ url +'" type="text/javascript"' + id + async + '></script>';
}
exports.cssUrl = function(obj) {
	var returnUrl = '';
	if ( obj.absolutUrl == true ) {
		returnUrl = obj.href;
	} else {
		returnUrl = '/' + config.cssDir + "/" + obj.href;
	}

	return returnUrl;
}

exports.print = function( string ) {
	if ( typeof( string ) == 'undefined' ) {
		string = '';
	}

	return string;
}

exports.pageTitle = function(page) {
	var title = page.title;
	if ( typeof(page.isHome) !== 'undefined' && page.isHome === true ) {
		title = config.name + config.separator + title;
	} else {
		title = title + config.separator + config.extendedName;
	}
	return title;
}

exports.getCoverDir = function( ) {
	return config.uploadDir + config.coverDirName + '/';
}

exports.getCoverUrl = function( fileName ) {
	return configEnv.url + '/' + config.uploadDirName + '/' + config.coverDirName + '/' + fileName;
}

exports.getPreEncoderDir = function() {
	return config.storageUploadDir + config.storageVideoPreEncodeDirName + '/';
}

exports.getWebseedDir = function() {
	return config.storageUploadDir + config.storageVideoDirName + '/';
}

exports.getWebseedUrl = function(filename) {
	return configEnv.storageUrl + '/' + config.storageVideoDirName + '/' + filename;
}

exports.getTorrentDir = function() {
	return config.storageUploadDir + config.storageTorrentDirName + '/';
}

exports.getTorrentUrl = function(filename) {
	return configEnv.storageUrl + '/' + config.storageTorrentDirName + '/' + filename;
}

exports.getDefaultSocialImage = function() {
	return configEnv.url + '/' + config.imgDir + '/' + config.defaultSocialImage;
}

exports.youtubeEmbedUrl = function(youtubeUrl) {
	var youtubeIdRegex = /youtu(?:.*\/v\/|.*v\=|\.be\/)([A-Za-z0-9_\-]{11})/i;
	var youtubeIdMatch = youtubeUrl.match(youtubeIdRegex);
	var embedUrl = 'https://www.youtube.com/embed/' + youtubeIdMatch[1];
	return embedUrl;
}

exports.getPartners = function() {
	return partners;
}

exports.getVideoLoadingImage = function() {
	return configEnv.url + '/' + config.imgDir + '/' + config.defaultLoadingVideoImage;
}

exports.notificationManifestUrl = function() {
	return configEnv.url + front.pages.notificationManifest.url;
}