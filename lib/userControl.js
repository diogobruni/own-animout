var config = require('../config/config'),
	env = process.env.NODE_ENV || 'development',
	configEnv = config[env];

/*var front = require('../config/front'),
	admin = require('../config/admin');*/

var utils = require('./utils');

var Mongoose = require('mongoose'),
	User = Mongoose.model('User');

exports.isLoggedMinLevel = function(sessionUser, minLevel, callback) {
	var objReturn = {
		statusCode: 0 // -1 = Error | 0 = Not logged in | 1 = OK | 2 = Not required level
	};
	if (
		typeof(sessionUser) == 'undefined' ||
		typeof(sessionUser.email) == 'undefined'
	) {
		objReturn.statusCode = 0; // Not logged in
		return callback(objReturn);
	} else {
		var email = sessionUser.email;
		var objUser = new User();
		objUser.getByEmail(email, function(err, rUser) {
			if (err) {
				objReturn.statusCode = -1; // Error
				return callback(objReturn);
			} else {
				objReturn.user = rUser;
				if ( rUser.level >= minLevel ) {
					objReturn.statusCode = 1; // Ok
					return callback(objReturn);
				} else {
					objReturn.statusCode = 2; // Not required level
					return callback(objReturn);
				}
			}
		});
	}
}