var ffmpeg = require('fluent-ffmpeg');

function checkSub(input, next) {
	ffmpeg.ffprobe(input, function(err, metadata) {
		var hasSub = false;
		for( i in metadata.streams ) {
			var stream = metadata.streams[i];
			if ( stream.codec_type == 'subtitle' && ( stream.codec_name == 'ass' || stream.profile == 'unknown' ) ) {
				hasSub = true;
				break;
			}
		}
		next(hasSub);
	});
}

function encodeWithSub(input, output, next) {
	var error = false;

	ffmpeg(input)
	.output(output)
	.format('mp4')
	.size('1280x720')
	.videoBitrate(800)
	.videoCodec('libx264')
	//.audioCodec('libfaac')
	.audioBitrate('96')
	.audioChannels(2)
	.audioFrequency(24000)
	.outputOptions('-vf subtitles=\''+ input + '\'')
	.on('progress', function(progress) {
	  //console.log('Processing: ' + Math.floor(progress.percent) + '% done');
	})
	.on('end', function(stdout, stderr) {
		//console.log('Finished');
		next(error, output);
	})
	.on('error', function(err, stdout, stderr) {
		//console.log('Cannot process video: ' + err.message);
		error = err;
		next(error, false);
	})
	.run();
}

function encodeWithoutSub(input, output, next) {
	var error = false;

	ffmpeg(input)
	.output(output)
	.format('mp4')
	.size('1280x720')
	.videoBitrate(800)
	.videoCodec('libx264')
	//.audioCodec('libfaac')
	.audioBitrate('96')
	.audioChannels(2)
	.audioFrequency(24000)
	.on('progress', function(progress) {
	  //console.log('Processing: ' + Math.floor(progress.percent) + '% done');
	})
	.on('end', function(stdout, stderr) {
		//console.log('Finished');
		next(error, output);
	})
	.on('error', function(err, stdout, stderr) {
		//console.log('Cannot process video: ' + err.message);
		error = err;
		next(error, false);
	})
	.run();
}

exports.encode = function(input, output, next) {
	checkSub(input, function(hasSub) {
		if ( hasSub ) {
			encodeWithSub(input, output, next);
		} else {
			encodeWithoutSub(input, output, next);
		}
	});
}