var needle = require('needle'),
	http = require('http'),
	validUrl = require('valid-url'),
	fs = require('fs'),
	path = require('path');

var url = "http://anitubebr.xpg.uol.com.br/video/88370/Hai-to-Gensou-no-Grimgar-09",
	regexTitle = /<title>(.*)<\/title>/,
	regexPage = /(http|https)(:\/\/)([\w.?=&\/]+)(config.+?[\w.?=&\/]+)/g,
	regexConfig = /http.*\.mp4/g,
	indexQuality = '_hd.mp4',
	destination = path.normalize(__dirname + '/test.mp4');

var video = {
	sd: {
		url: ''
	},
	hd: {
		url: ''
	}
};

needle.defaults({
	open_timeout: 60000,
	user_agent: 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36',
	compressed: true,
	follow_max: 5
});

if ( validUrl.isUri(url) ) {
	needle.get(url, function(errPage, respPage) {
		if (errPage) {
			console.log(errPage);
			return false;
		}

		var resultPage = respPage.body.match(regexPage);
		var configUrl = resultPage.join();

		var auxPageTitle = respPage.body.match(regexTitle);
		auxPageTitle = auxPageTitle[1];
		auxPageTitle = auxPageTitle.split(' - ');
		var pageTitle = auxPageTitle[0];
		console.log(pageTitle);

		if ( validUrl.isUri(configUrl) ) {
			var options = {
				compressed: true,
				follow_max: 5
			};

			var configStream = needle.get(configUrl, options),
				videoUrls,
				configContent;

			configStream.on('readable', function() {
				var data;
				while(data=this.read()) {
					configContent += data.toString();
				}
			})
			.on('end', function() {
				var videoUrls;
				videoUrls = configContent.match(regexConfig);

				for( i in videoUrls ) {
					var currentVideoUrl = videoUrls[i];
					if ( validUrl.isUri(currentVideoUrl) ) {
						if ( ~currentVideoUrl.indexOf(indexQuality) ) {
							video.hd.url = currentVideoUrl;
						} else {
							video.sd.url = currentVideoUrl;
						}
					} else {
						console.log('Invalid video url: ', currentVideoUrl)
					}
				}

				var downloadThis;

				if ( video.hd.url ) {
					downloadThis = video.hd.url;
				} else if ( video.sd.url ) {
					downloadThis = video.sd.url;
				}
				download(downloadThis, destination, function() {
					console.log('Arquivo baixado!');
				});
			});
		} else {
			console.log('Invalid config url: ', configUrl);
		}
	});
} else {
	console.log('Invalid url: ', url);
}

var download = function(url, dest, cb) {
	console.log('Baixando: ', url);

	var file = fs.createWriteStream(dest),
		options = {
			follow_max: 5
		};
	
	needle.get(url, options).pipe(file);

	file.on('finish', function() {
		file.close(cb);
	});
};