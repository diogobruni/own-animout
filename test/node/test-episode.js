require('../models/episode.js');
require('../models/serial.js');

var env = process.env.NODE_ENV || 'development',
	config = require('../config/config')[env],
	Mongoose = require('mongoose'),
	Episode = Mongoose.model('Episode');

var connect = function() {
	var options = { server: { socketOptions: { keepAlive: 1 } } }
	Mongoose.connect(config.mongo.url, options);
}
connect();

var test = new Episode();
test.test(function(err, result) {
	test.populateThis(result, function(err, pResult) {
		console.log(pResult);
	});
});