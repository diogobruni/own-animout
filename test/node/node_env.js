var http = require('http'),
	env = process.env.NODE_ENV || 'production',
	config = require('./config')[env];

console.log(config);

http.createServer(function (req, res) {
	res.writeHead(200, {'Content-Type': 'text/plain'});
	res.end(env);
}).listen(3004);
console.log('Server running at http://localhost:3004/');