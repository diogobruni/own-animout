use dev_animout;
db.episodes.aggregate([
	{
		$project: {
			title: 1,
			number: 1,
			type: 1,
			viewsCount: { $size: "$views" }
		}
	},
	{
		$match: {
			//_owner: ObjectId("56ee50cb8be664ac3466413b"),
			viewsCount: {
				$gte: 50
			}
			//$or: [
			//	{ "type.normal": true },
			//	{ "type.filler": true }
			//]
		}
	},
	{
		$sort: {
			number: 1,
		}
	},
	{
		$limit: 20
	}
])
