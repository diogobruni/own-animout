use dev_animout;

var mapFunction = function() {
	var key = this._id,
		value = {
			title: this.title,
			titleLength: this.title.length,
			viewsCount: this.views.length
		};

	emit(key, value);
}

var reduceFunction = function(key, value) {
	return value;
}

db.episodes.mapReduce(
	mapFunction,
	reduceFunction,
	{
		query: {
			_owner: ObjectId("56ee50cb8be664ac3466413b")
		},
		out: { "inline" : 1 }
	}
)
//56ee50cb8be664ac3466413b