use dev_animout;
db.episodes.aggregate([
	{
		$match: {
			//_id: { $gt: ObjectId("56ef623ac83bdbea3c6b8881") },
			number: { $gt: 1 },
			_owner: ObjectId("56ee50cb8be664ac3466413b"),
			"type.ova": true
		}
	},
	{
		$project: {
			title: 1,
		}
	},
	{
		$sort: {
			title: 1,
		}
	},
	{
		$limit: 5
	}
])
