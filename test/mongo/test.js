use dev_animout;
//db.episodes.find({
//	_owner: ObjectId("56ee50cb8be664ac3466413b"),
//	"type.ova": true
//}).pretty()

db.serials.aggregate(
	{ $match: { genre: { $in: [ '3', '5', '29', '30', '35', '36' ] }, status: true } },
	{ $unwind: "$genre" },
	{ $match: { genre: { $in: [ '3', '5', '29', '30', '35', '36' ] }, status: true } },
	{ $group: {
		_id: {
			_id: '$_id',
			title: '$title',
			slug: '$slug',
			cover: '$cover'
		},
		matches: { $sum: 1 }
	}},
	{ $sort: { matches: -1 } },
	{ $limit: 4 }
)
