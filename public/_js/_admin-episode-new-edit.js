jQuery(function() {
	if ( jQuery('body').hasClass('episode-new-edit') ) {
		var divProgressBar = jQuery('.divProgressBar'),
			divLoading = jQuery('.divLoading'),
			progressBar = jQuery('.progress-bar', divProgressBar);

		// Remote Select2 Anime
		jQuery('#owner').select2({
			ajax: {
				url: ajaxurl,
				type: 'POST',
				dataType: 'json',
				cache: true,
				data: function(params) {
					return {
						action: 'ajax-get-serials',
						q: params.term
					};
				},
				proccessResults: function( data, params ) {
					return {
	                    results: data.results
	                };
				},
			},
			templateResult: function ( item ) {
				if ( item.text ) return item.text;
	            return item.name;
	        },
	        templateSelection: function ( item ) {
	        	if ( item.text ) return item.text;
	            return item.name;
	        },
	        theme: "bootstrap",
	        placeholder: "Selecione o anime"
		})
		.on("select2:select", function (e) {
        	jQuery('[name=_owner]').val( e.params.data.id );

        	ownerSelectChange({
        		id: e.params.data.id,
        		name: e.params.data.name
        	});
        	
    	})
    	.on("select2:unselect", function (e) {
        	jQuery('[name=_owner]').val('');
    	});

    	// FTP Select2 Files
    	jQuery('#ftpFile').select2({
			ajax: {
				url: ajaxurl,
				type: 'POST',
				dataType: 'json',
				cache: true,
				data: function(params) {
					return {
						action: 'ajax-get-ftp-files',
						q: params.term
					};
				},
				proccessResults: function( data, params ) {
					return {
	                    results: data.results
	                };
				},
			},
			templateResult: function ( item ) {
				if ( item.text ) return item.text;
	            return item.name;
	        },
	        templateSelection: function ( item ) {
	        	if ( item.text ) return item.text;
	            return item.name;
	        },
	        theme: "bootstrap",
	        placeholder: "Selecione o arquivo"
		})
		.on("select2:select", function (e) {
        	
    	})
    	.on("select2:unselect", function (e) {
    	});

    	// Update episode title by the anime selected
    	var lastOwner = false;
    	function ownerSelectChange(object) {
    		if (!object) {
    			object = {
    				id: jQuery('#owner').val(),
    				name: jQuery('#owner option:selected').text()
    			}
    		}

    		if (object.name) {
	    		var currentOwner = jQuery('[name=title]').val(),
	        		newOwner = object.name
	        	if ( currentOwner == '' || currentOwner == lastOwner ) {
	        		lastOwner = newOwner;
	        		jQuery('[name=title]').val(newOwner).trigger('change');
	        	}
    		}
    	}
    	ownerSelectChange();

    	// Update episode number by episode title
    	var updateEpisodeNumber = function() {
    		var title = jQuery('[name=title]').val(),
    			regex = /(\d+(\.\d+)?)/,
    			number = '';

			var match = title.match(regex);
			console.log(match);
			if ( match ) {
				number = parseInt(match[0]);
			}

			jQuery('[name=number]').val(number).trigger('change');
    	};
    	jQuery('[name=title]').keyup(updateEpisodeNumber).change(updateEpisodeNumber);

		var domHasMedia = jQuery('[name=hasMedia]');
	    function manageMediaBox() {
	        if ( domHasMedia && jQuery(domHasMedia).val() == 1 ) {
	            jQuery('.boxSetMedia').slideUp('fast');
	            jQuery('.boxHasMedia').slideDown('fast');
	        } else {
	            jQuery('.boxHasMedia').slideUp('fast');
	            jQuery('.boxSetMedia').slideDown('fast');
	        }
	    }
	    manageMediaBox();

	    if ( jQuery('[name=youtubeUrl]').val() != '' ) {
	    	jQuery('a[href="#youtubeWay"]').click();
	    }

	    jQuery('.boxHasMedia .changeMedia').click(function() {
	        jQuery(domHasMedia).val(0);
	        jQuery('.fileText').val('');
	        jQuery('.previewVideoPlayer').remove();
	        manageMediaBox();
	    });

	    jQuery('#adminFormEpisodeNewEdit').validate({
	        rules: {
	            owner: {
	                required: true
	            },
	            title: {
	                required: true
	            },
	            number: {
	            	required: true
	            },
	            type: {
	            	required: true
	            },
	            media: {
	                required: false
	            },
	            tags: {
	                required: false
	            },
	            description: {
	                required: false
	            }
	        },

	        messages: {
	            owner: {
	                required: 'Selecione o anime a qual este episódio pertence.'
	            },
	            title: {
	                required: 'O título é obrigatório.'
	            },
	            number: {
	                required: 'O número do episódio é obrigatório.'
	            },
	            type: {
	                required: 'O tipo do episódio é obrigatório.'
	            }
	        },

	        submitHandler: function ( form ) {
	        	var btnSubmit = jQuery('[type=submit]');
	            var formData = new FormData( form );

	    		jQuery('.formSuccessBox, .formErrorBox').addClass('hide');
	    		var ajax = jQuery.ajax({
	    			url: ajaxurl,
	    			type: 'POST',
	                //xhr: progressHandlingFunction,
	                xhr: function() {
	                	var xhr = jQuery.ajaxSettings.xhr();
				        xhr.upload.onprogress = function(e) {
				        	var percentComplete = Math.floor(e.loaded / e.total * 100);
				        	jQuery(progressBar).css({
								width: percentComplete + '%'
							});
				        };

				        xhr.upload.onload = function(e) {
				        	if ( jQuery('[name=media]').val() != '' ) {
					        	ajax.abort();
					        	bootbox.alert('Arquivo enviado, encode em andamento');
				        	}
				        };

				        return xhr;
	                },
	                dataType: 'JSON',
	    			data: formData,
	                //dataType: 'JSON',
	                cache: false,
	                contentType: false,
	                processData: false,
	                timeout: ( 1000 * 60 ) * 120,
	                beforeSend: function() {
	                	btnSubmit.attr('disabled', true);

	                	if ( jQuery('[name=media]').val() != '' ) {
	                		jQuery(divProgressBar).show();
	                	} else if ( jQuery('[name=hotlinkUrl]').val() != '' ) {
	                		jQuery(divLoading).show();
	                	} else if ( jQuery('[name=youtubeUrl]').val() != '' ) {
	                		jQuery(divProgressBar).show();
	                	}
	                },
	    			success: function(data) {
	    				btnSubmit.attr('disabled', true);
	    				switch( data.status ) {
	    					case true:
	    						window.location.href = data.redirectTo;
	    					break;
	    					case false:
	                            if ( Object.keys( data.errors ).length ) {
	                                jQuery(form).validate().showErrors( data.errors );
	                            }

	                            if ( data.message ) {
	    						  jQuery('.formErrorBox').removeClass('hide').find('.textMessage').html( data.message );
	                            }
	    					default:
	    						btnSubmit.removeAttr('disabled');
	    						jQuery(divProgressBar).hide();
	    						jQuery(divLoading).hide();
	    					break;
	    				}
	    			}
	    		});
	        }
	    });
	}
});