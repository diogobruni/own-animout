var isPushEnabled = false;

// Once the service worker is registered set the initial state
function initialiseState() {
	// Are Notifications supported in the service worker?
	if (!('showNotification' in ServiceWorkerRegistration.prototype)) {
		console.warn('Notifications aren\'t supported.');
		return;
	}

	// Check the current Notification permission.
	// If its denied, it's a permanent block until the
	// user changes the permission
	if (Notification.permission === 'denied') {
		console.warn('The user has blocked notifications.');
		return;
	}

	// Check if push messaging is supported
	if (!('PushManager' in window)) {
		console.warn('Push messaging isn\'t supported.');
		return;
	}

	// We need the service worker registration to check for a subscription
	navigator.serviceWorker.ready.then(function(serviceWorkerRegistration) {
		// Do we already have a push message subscription?
		serviceWorkerRegistration.pushManager.getSubscription()
		.then(function(subscription) {
		// Enable any UI which subscribes / unsubscribes from
		// push messages.
		var pushButton = document.querySelector('.js-push-button');
		pushButton.disabled = false;

		if (!subscription) {
			// We aren't subscribed to push, so set UI
			// to allow the user to enable push
			return;
		}

		// Keep your server in sync with the latest subscriptionId
		sendSubscriptionToServer(subscription);

		// Set your UI to show they have subscribed for
		// push messages
		pushButton.textContent = 'Disable Push Messages';
			isPushEnabled = true;
		})
		.catch(function(err) {
			console.warn('Error during getSubscription()', err);
		});
	});  
}

function subscribe() {
	// Disable the button so it can't be changed while
	// we process the permission request
	var pushButton = document.querySelector('.js-push-button');
	pushButton.disabled = true;

	navigator.serviceWorker.ready.then(function(serviceWorkerRegistration) {
		serviceWorkerRegistration.pushManager.subscribe({
			userVisibleOnly: true
		})
		.then(function(subscription) {
			// The subscription was successful
			isPushEnabled = true;
			pushButton.textContent = 'Disable Push Messages';
			pushButton.disabled = false;

			// TODO: Send the subscription.endpoint to your server
			// and save it to send a push message at a later date
			return sendSubscriptionToServer(subscription);
		})
		.catch(function(e) {
			if (Notification.permission === 'denied') {
				// The user denied the notification permission which
				// means we failed to subscribe and the user will need
				// to manually change the notification permission to
				// subscribe to push messages
				console.warn('Permission for Notifications was denied');
				pushButton.disabled = true;
			} else {
				// A problem occurred with the subscription; common reasons
				// include network errors, and lacking gcm_sender_id and/or
				// gcm_user_visible_only in the manifest.
				console.error('Unable to subscribe to push.', e);
				pushButton.disabled = false;
				pushButton.textContent = 'Enable Push Messages';
			}
		});
	});
}

window.addEventListener('load', function() {
	var pushButton = document.querySelector('.js-push-button');
	pushButton.addEventListener('click', function() {
		if (isPushEnabled) {
			//unsubscribe();
		} else {
			subscribe();
		}
	});

	// Check that service workers are supported, if so, progressively
	// enhance and add push messaging support, otherwise continue without it.
	if ('serviceWorker' in navigator) {
		navigator.serviceWorker.register('/js/service-worker.js')
		.then(initialiseState);
	} else {
		console.warn('Service workers aren\'t supported in this browser.');
	}
});

/*
var OneSignal = OneSignal || [];

OneSignal.push(["init", {
	appId: oneSignalAppId,
	autoRegister: true,
	path: 'https://animout.com.br/js/',
	notifyButton: {
		enable: true, // Set to false to hide
		size: 'medium', // One of 'small', 'medium', or 'large'
	    theme: 'default', // One of 'default' (red-white) or 'inverse" (white-red)
	    position: 'bottom-left', // Either 'bottom-left' or 'bottom-right'
	    prenotify: true, // Show an icon with 1 unread message for first-time site visitors
	    showCredit: false, // Hide the OneSignal logo
	    text: {
	      'tip.state.unsubscribed': 'Se inscreva para receber notificações das notícias.',
	      'tip.state.subscribed': "Agora você receberá as notícias em primeira mão!",
	      'tip.state.blocked': "Suas notificações estão bloqueadas.",
	      'message.prenotify': 'Clique para receber as notícias em primeira mão!',
	      'message.action.subscribed': "Obrigado, fico feliz de poder te notificar de novas notícias!",
	      'message.action.resubscribed': "Fico feliz de poder te notificar de novas notícias novamente!",
	      'message.action.unsubscribed': "Que triste, não poderei mais te notificar de novas notícias.",
	      'dialog.main.title': 'Configurações',
	      'dialog.main.button.subscribe': 'RECEBER',
	      'dialog.main.button.unsubscribe': 'DEIXAR DE RECEBER',
	      'dialog.blocked.title': 'Desbloqueie as notificações',
	      'dialog.blocked.message': "Siga as instruções para permitir as notificações:"
		}
	}
}]);
*/
/*
OneSignal.push( function() {
    var oneSignal_options = {};

	oneSignal_options['appId'] = oneSignalAppId;
	oneSignal_options['autoRegister'] = true;
	oneSignal_options['welcomeNotification'] = { };
	oneSignal_options['welcomeNotification']['title'] = "Animout";
	oneSignal_options['welcomeNotification']['message'] = "Obrigado por se inscrever!";
	oneSignal_options['welcomeNotification']['url'] = "https://animout.com.br";
	oneSignal_options['path'] = "https://animout.com.br/js/";
	oneSignal_options['notifyButton'] = { };
	oneSignal_options['notifyButton']['enable'] = true;
	oneSignal_options['notifyButton']['position'] = 'bottom-left';
	oneSignal_options['notifyButton']['theme'] = 'default';
	oneSignal_options['notifyButton']['size'] = 'medium';
	oneSignal_options['notifyButton']['prenotify'] = true;
	oneSignal_options['notifyButton']['showCredit'] = false;
	oneSignal_options['notifyButton']['text'] = {};
	oneSignal_options['notifyButton']['text']['message.prenotify'] = 'Clique para receber as notícias em primeira mão!';
	oneSignal_options['notifyButton']['text']['tip.state.unsubscribed'] = 'Se inscreva para receber notificações das notícias.';
	oneSignal_options['notifyButton']['text']['tip.state.subscribed'] = 'Agora você receberá as notícias em primeira mão!';
	oneSignal_options['notifyButton']['text']['tip.state.blocked'] = 'Suas notificações estão bloqueadas.';
	oneSignal_options['notifyButton']['text']['message.action.subscribed'] = 'Obrigado, fico feliz de poder te notificar de novas notícias!';
	oneSignal_options['notifyButton']['text']['message.action.resubscribed'] = 'Fico feliz de poder te notificar de novas notícias novamente!';
	oneSignal_options['notifyButton']['text']['message.action.unsubscribed'] = 'Que triste, não poderei mais te notificar de novas notícias.';
	oneSignal_options['notifyButton']['text']['dialog.main.title'] = 'Configurações';
	oneSignal_options['notifyButton']['text']['dialog.main.button.subscribe'] = 'RECEBER';
	oneSignal_options['notifyButton']['text']['dialog.main.button.unsubscribe'] = 'DEIXAR DE RECEBER';
	oneSignal_options['notifyButton']['text']['dialog.blocked.title'] = 'Desbloqueie as notificações';
	oneSignal_options['notifyButton']['text']['dialog.blocked.message'] = 'Siga as instruções para permitir as notificações:';

    OneSignal.init(oneSignal_options);
});
*/