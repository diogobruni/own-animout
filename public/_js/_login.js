jQuery.material.init();

jQuery('.form-signin').validate({
    rules: {
        email: {
            required: true
        },
        password: {
            required: true
        },
        remember: {
            required: false
        }
    },

    messages: {
        email: {
            required: "O e-mail é obrigatório."
        },
        password: {
            required: "A senha é obrigatória."
        }
    },

    /*
    invalidHandler: function (event, validator) { //display error alert on form submit   
        jQuery('.alert-danger', jQuery('.login-form')).show();
    },

    highlight: function (element) { // hightlight error inputs
        jQuery(element).closest('.form-group').addClass('has-error'); // set error class to the control group
    },

    success: function (label) {
        label.closest('.form-group').removeClass('has-error');
        label.remove();
    },

    errorPlacement: function (error, element) {
        error.insertAfter(element.closest('.input-icon'));
    },
    */

    submitHandler: function (form) {
		jQuery('.form-message').hide();
		jQuery.ajax({
			url: ajaxurl,
			type: 'POST',
			data: jQuery(form).serialize(),
            beforeSend: function() {
                jQuery('.form-signin .alert-danger').hide();
            },
			success: function(json) {
				var data = eval('('+ json +')');
				switch(data.status) {
					case true:
						window.location.href = jQuery(form).attr('action');
					break;
					case false:
						jQuery('.form-signin .alert-danger span:first').html(data.message).parents('.alert-danger').show();
					break;
				}
			}
		});
    }
});

jQuery('.login-form input').keypress(function (e) {
    if (e.which == 13) {
        if (jQuery('.login-form').validate().form()) {
            jQuery('.login-form').submit(); //form validation success, call ajax form submit
        }
        return false;
    }
});