var OneSignal = OneSignal || [];

OneSignal.push(["init", {
	appId: notification.appId,
	autoRegister: true,
	welcomeNotification: {
		disabled: true,
		title: site.name,
		message: 'Obrigado por se inscrever!',
	},
	notifyButton: {
		enable: true, // Set to false to hide
		size: 'medium', // One of 'small', 'medium', or 'large'
		theme: 'default', // One of 'default' (red-white) or 'inverse" (white-red)
		position: 'bottom-left', // Either 'bottom-left' or 'bottom-right'
		prenotify: true, // Show an icon with 1 unread message for first-time site visitors
		showCredit: false, // Hide the OneSignal logo
	    text: {
			'tip.state.unsubscribed': 'Se inscreva para receber notificações das notícias.',
			'tip.state.subscribed': "Agora você receberá as notícias em primeira mão!",
			'tip.state.blocked': "Suas notificações estão bloqueadas.",
			'message.prenotify': 'Clique para receber as notícias em primeira mão!',
			'message.action.subscribed': "Obrigado, fico feliz de poder te notificar de novas notícias!",
			'message.action.resubscribed': "Fico feliz de poder te notificar de novas notícias novamente!",
			'message.action.unsubscribed': "Que triste, não poderei mais te notificar de novas notícias.",
			'dialog.main.title': 'Configurações',
			'dialog.main.button.subscribe': 'RECEBER',
			'dialog.main.button.unsubscribe': 'DEIXAR DE RECEBER',
			'dialog.blocked.title': 'Desbloqueie as notificações',
			'dialog.blocked.message': "Siga as instruções para permitir as notificações:"
		}
	}
}]);