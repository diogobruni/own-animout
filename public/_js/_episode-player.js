jQuery(function() {
	if ( jQuery('body').hasClass('episode') ) {
		var bWebRTC = WebTorrent.WEBRTC_SUPPORT;

		if ( jQuery('.boxVideoPlayer').length ) {
			jQuery('.boxVideoPlayer').each(function() {
				var playerObj = jQuery('>video:first', this);
				if ( !playerObj ) {
					console.log('No video tag found!');
					return false;
				}

				var videoAd = new videoAdManager(playerObj);
				if ( jQuery('.midrol-1').length ) {
					videoAd.setAdTime( function() {
						jQuery('.midrol-1').fadeIn('fast');
					}, 15);
				}

				if ( jQuery('.midrol-2').length ) {
					videoAd.setAdTime( function() {
						jQuery('.midrol-2').fadeIn('fast');
					}, 60);
				}

				videoAd.startWhenReady();

				var torrentFile = jQuery(playerObj).attr('data-torrent-file');
				var videoFile = jQuery(playerObj).attr('data-video-file');
				jQuery(playerObj).removeAttr('data-torrent-file');
				jQuery(playerObj).removeAttr('data-video-file');

				var isPreroll = Boolean(jQuery(playerObj).attr('data-preroll'));
				var jsVideoObj = videojs(playerObj[0], {fluid:true}, function() {
					this.hotkeys({
						alwaysCaptureHotkeys: true
					});

					if ( isPreroll ) {
						this.vastClient({
							//Media tag URL
							adTagUrl: "https://www.objectdisplay.com/a/display.php?r=1164026&acp=pre",
							playAdAlways: true,
							//Note: As requested we set the preroll timeout at the same place than the adsCancelTimeout
							adCancelTimeout: 3000,
							adsEnabled: true,
							vpaidFlashLoaderPath: "/swf/VPAIDFlash.swf"
					    });
					}

					jQuery('.boxVideoPlayer > .video-js').append(jQuery('.midrol'));
				});

				if ( bWebRTC ) {
					var client = new WebTorrent();
					client.add(torrentFile, function(torrent) {
						var file = torrent.files[0];
						jQuery(file).attr('id', 'peerVideo')

						file.renderTo(playerObj[0], function(err, elem) {
							if (err) { console.log(err); }
						});
					});
				} else {
					jsVideoObj.src([
						{ type: "video/mp4", src: videoFile },
					]);
				}

				jsVideoObj.persistvolume({ namespace: 'animout-volume' });
			});

			jQuery('.ad-close', '.midrol').click(function() {
				jQuery(this).parents('.midrol').fadeOut('fast');
			});
		}

		if ( jQuery('.boxYoutubePlayer').length ) {
			
		}
	}
});

function videoAdManager(playerObj) {
	var player = playerObj;
	var timeEvents = [];

	var runAtTime = function(handler, time) {
		var wrapped = function() {
			if (this.currentTime >= time) {
				jQuery(this).off('timeupdate', wrapped);
				return handler.apply(this, arguments);
			}
		}
    	return wrapped;
	}

	this.setAdTime = function(handler, moment) {
		timeEvents.push({
			handler: handler,
			moment: moment
		});
	}

	this.startWhenReady = function() {
		jQuery(player).on('loadedmetadata', function() {
			var duration = this.duration;

			for( i in timeEvents ) {
				var event = timeEvents[i];
				var time = duration / 100 * event.moment;
				jQuery(this).on('timeupdate', runAtTime(event.handler, time));
			}
		});
	}
}