/* History Functions */
(function(history){
    var pushState = history.pushState;
    history.pushState = function(state) {
        if (typeof history.onpushstate == "function") {
            history.onpushstate({state: state});
        }
        // whatever else you want to do
        // maybe call onhashchange e.handler
        return pushState.apply(history, arguments);
    }
})(window.history);

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function showSplashScreen() {
	jQuery('.splash-screen').fadeIn('50');
}
function hideSplashScreen() {
	jQuery('.splash-screen').stop().hide();
}

function afterChangeContent() {
	jQuery('[data-toggle="tooltip"]').tooltip();
}
afterChangeContent();

function ajaxPage(url) {
	window.history.pushState(url, "", url);
}

internalAjax = false;
function ajaxLoadPage(url) {
	var urlQuerySymbol = !~url.indexOf('?') ? '?' : '&'
	var ajaxUrl = url + urlQuerySymbol + 'ajax=1';
	
	if ( internalAjax )	internalAjax.abort();
	internalAjax = jQuery.ajax({
		url: ajaxUrl,
		type: 'GET',
		beforeSend: function() {
			showSplashScreen();
		},
		error: function(jqXHR, textStatus) {
			hideSplashScreen();
			console.log(jqXHR.statusText);
			//console.log(textStatus);
			history.back();
		},
		success: function(data) {
			hideSplashScreen();
			jQuery(window).unbind('scroll');
			jQuery(window).scrollTop(0);
			//jQuery('body > .container').replaceWith(data);
			jQuery('body > .ajaxContainer').html(data);
			afterChangeContent();
		}
	});
}

window.onpopstate = history.onpushstate = function(e) {
	var url = e.state || window.location.href;
	ajaxLoadPage(url);
};

jQuery(document).ready(function() {
	jQuery('body').on('click', 'a.internalUrl', function(e) {
		e.preventDefault();

		var url = jQuery(this).attr('href');
		ajaxPage(url);
	});
});