// Adapta o jQuery Validator ao Bootstrap 3
jQuery.validator.setDefaults({
    errorElement: "span",
    errorClass: "help-block",
    highlight: function (element, errorClass, validClass) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
            //error.insertAfter(element.parent());
            element.parent().parent().append(error);
        } else {
            //error.insertAfter(element);
            element.parent().append(error);
        }
    }
});

// Funções para o data tables
function dTzeroRecords() {
    return '<span style="color: #a94442;">Nenhum registro encontrado no intervalo selecionado</span>';
}

function dTinfoEmpty() {
    return 'Nenhum registro encontrado';
}

function dTinfoFiltered() {
    return '(Total de _MAX_ registro(s) pesquisado(s))';
}
