jQuery(function() {
	if ( jQuery('body').hasClass('user-list') ) {
		var action = jQuery('#manageThisTable').attr('data-action');
		dataTable = jQuery('#manageThisTable').dataTable( {
	    	//"multipleSelection": true,
	    	"bSort": false,
	        "aLengthMenu": [
	            [50, 100, 300, 500],
	            [50, 100, 300, 500] // change per page values here
	        ],
	        "iDisplayLength": 100,
	        //"sPaginationType": "bootstrap",
	        "language": {
	        	"processing": "Processando",
	        	"lengthMenu": "Exibir _MENU_ itens por página",
	            "zeroRecords": dTzeroRecords(),
	            "info": "Exibindo de _START_ até _END_ de _TOTAL_ registro(s)",
	            "infoEmpty": dTinfoEmpty(),
	            "infoFiltered": dTinfoFiltered(),
	            "oPaginate": {
	                "sPrevious": "Anterior",
	                "sNext": "Próximo"
	            },
	            "search": "Procurar"
	        },
	        "processing": true,
	        "serverSide": false,
	        "ajax": {
	            "url": ajaxurl,
	            "type": "POST",
	            "data": function (d) {
	            	d.action = action;
	            }
	        },
	        "columns": [
	            { "data": "nickname" },
	            { "data": "level" },
	            { "data": "email" },
	            { "data": "actions" }
	        ],
	        "fnDrawCallback": function() {
	        	//jQuery('[type=checkbox]').uniform();
	        }
	    } );

		jQuery('#manageThisTable').on('click', 'tbody tr .deleteItem', function(){
			if (dataTable) {
				var tr = jQuery(this).parents('tr');
				var trIndex = jQuery(this).parents('tr').index();
				var id = jQuery(tr).attr('id');

				jQuery(tr).addClass('highlight');

				var confirmMessage = 'Confirma a exclusão do item em destaque ?';
				bootbox.confirm( confirmMessage, function(result) {
					if ( result ) {
						jQuery.ajax({
							url: ajaxurl,
							type: 'POST',
							data: { action: jQuery('#manageThisTable').attr('data-delete-action'), id: id },
							dataType: 'JSON',
							success: function( data ) {
								if ( data.status ) {
									dataTable.fnDeleteRow(trIndex);
			                        /*
									var page = dataTable.page();
			                        dataTable.ajax.reload(function() {
			                            dataTable.page( page ).draw(false);
			                        });
									*/
								} else {
									if ( data.message ) {
										bootbox.alert( data.message );
									}
								}
							}
						});
					} else {
						jQuery(tr).removeClass('highlight');
					}

				});
			}
		});
	}
});