jQuery(function() {
	if ( jQuery('body').hasClass('serial-new-edit') ) {
		var divProgressBar = jQuery('.divProgressBar'),
			progressBar = jQuery('.progress-bar', divProgressBar);

		var domHasImage = jQuery('[name=hasImage]');
	    function manageImageBox() {
	        if ( domHasImage && jQuery(domHasImage).val() == 1 ) {
	            jQuery('.boxSetImage').slideUp('fast');
	            jQuery('.boxHasImage').slideDown('fast');
	        } else {
	            jQuery('.boxHasImage').slideUp('fast');
	            jQuery('.boxSetImage').slideDown('fast');
	        }
	    }
	    manageImageBox();

	    jQuery('.boxHasImage .changeImage').click(function() {
	        jQuery(domHasImage).val(0);
	        jQuery('.fileText').val('');
	        manageImageBox();
	    });

	    jQuery('#adminFormSerialNewEdit').validate({
	        rules: {
	            title: {
	                required: true
	            },
	            cover: {
	                required: false
	            },
	            genre: {
	                required: false
	            },
	            synopsis: {
	                required: false
	            }
	        },

	        messages: {
	            title: {
	                required: 'O título é obrigatório.'
	            }
	        },

	        submitHandler: function ( form ) {
	            var formData = new FormData( form );

	    		jQuery('.formSuccessBox, .formErrorBox').addClass('hide');
	    		jQuery.ajax({
	    			url: ajaxurl,
	    			type: 'POST',
	                dataType: 'JSON',
	                xhr: progressHandlingFunction,
	    			data: formData,
	                //dataType: 'JSON',
	                cache: false,
	                contentType: false,
	                processData: false,
	                 beforeSend: function() {
	                	jQuery(divProgressBar).show();
	                },
	    			success: function(data) {
	    				switch( data.status ) {
	    					case true:
	    						window.location.href = data.redirectTo;
	    					break;
	    					case false:
	                            if ( Object.keys( data.errors ).length ) {
	                                jQuery(form).validate().showErrors( data.errors );
	                            }

	                            if ( data.message ) {
	    						  jQuery('.formErrorBox').removeClass('hide').find('.textMessage').html( data.message );
	                            }
	    					break;
	    				}
	    			}
	    		});
	        }
	    });

		function progressHandlingFunction( e ) {
			var xhr = new window.XMLHttpRequest();

		    xhr.upload.addEventListener("progress", function(evt) {
				if (evt.lengthComputable) {
					var percentComplete = evt.loaded / evt.total;
					percentComplete = parseInt(percentComplete * 100);
					jQuery(progressBar).css({
						width: percentComplete + '%'
					});

					if (percentComplete === 100) {}
				}
		    }, false);

		    return xhr;
	    }
	}
});