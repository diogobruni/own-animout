jQuery(function() {
	if ( jQuery('body').hasClass('user-new-edit') ) {

		var divProgressBar = jQuery('.divProgressBar'),
			divLoading = jQuery('.divLoading'),
			progressBar = jQuery('.progress-bar', divProgressBar);

	    jQuery('#adminFormUserNewEdit').validate({
	        rules: {
	            email: {
	                required: true,
	                email: true
	            },
	            nickname: {
	                required: true
	            },
	            level: {
	            	required: true
	            },
	            password: {
	                required: false
	            },
	            passwordRepeat: {
	                required: false,
	                equalTo: '#password'
	            }
	        },

	        messages: {
	            email: {
	                required: 'Preencha o e-mail corretamente.',
	                email: 'O e-mail digitado é inválido'
	            },
	            nickname: {
	                required: 'O apelido é obrigatório.'
	            },
	            level: {
	            	required: 'Selecione um nível de permissão.'
	            },
	            passwordRepeat: {
	                equalTo: 'As senhas digitadas devem ser idênticas.'
	            }
	        },

	        submitHandler: function ( form ) {
	        	var btnSubmit = jQuery('[type=submit]');
	            var formData = new FormData( form );

	    		jQuery('.formSuccessBox, .formErrorBox').addClass('hide');
	    		jQuery.ajax({
	    			url: ajaxurl,
	    			type: 'POST',
	    			dataType: 'JSON',
	    			data: formData,
	    			
	    			cache: false,
	                contentType: false,
	                processData: false,
	                
	    			success: function(data) {
	    				btnSubmit.attr('disabled', true);
	    				switch( data.status ) {
	    					case true:
	    						window.location.href = data.redirectTo;
	    					break;
	    					case false:
	                            if ( Object.keys( data.errors ).length ) {
	                                jQuery(form).validate().showErrors( data.errors );
	                            }

	                            if ( data.message ) {
									jQuery('.formErrorBox').removeClass('hide').find('.textMessage').html( data.message );
	                            }
	    					break;
	    				}
	    			}
	    		});
	        }
	    });
	}
});