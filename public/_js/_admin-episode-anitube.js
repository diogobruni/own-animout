jQuery(function() {
	if ( jQuery('body').hasClass('episode-new-anitube') ) {
		var divLoading = jQuery('.divLoading')

		jQuery('#owner').select2({
			ajax: {
				url: ajaxurl,
				type: 'POST',
				dataType: 'json',
				cache: true,
				data: function(params) {
					return {
						action: 'ajax-get-serials',
						q: params.term
					};
				},
				proccessResults: function( data, params ) {
					return {
	                    results: data.results
	                };
				},
			},
			templateResult: function ( item ) {
				if ( item.text ) return item.text;
	            return item.name;
	        },
	        templateSelection: function ( item ) {
	        	if ( item.text ) return item.text;
	            return item.name;
	        },
	        theme: "bootstrap",
	        placeholder: "Selecione o anime"
		})
		.on("select2:select", function (e) {
        	jQuery('[name=_owner]').val( e.params.data.id );
    	})
    	.on("select2:unselect", function (e) {
        	jQuery('[name=_owner]').val('');
    	});

	    jQuery('#adminFormEpisodeNewAnitube').validate({
	        rules: {
	            owner: {
	                required: true
	            },
	            episodeUrlList: {
	                required: true
	            }
	        },

	        messages: {
	            owner: {
	                required: 'Selecione o anime.'
	            },
	            episodeUrlList: {
	                required: 'Preencha com pelo menos uma url de episódio.'
	            }
	        },

	        submitHandler: function ( form ) {
	        	var btnSubmit = jQuery('[type=submit]');
	            //var formData = new FormData( form );
	            var formData = jQuery(form).serialize();

	    		jQuery('.formSuccessBox, .formErrorBox').addClass('hide');
	    		jQuery.ajax({
	    			url: ajaxurl,
	    			type: 'POST',
	                dataType: 'JSON',
	    			data: formData,
	                //dataType: 'JSON',
	                timeout: ( 1000 * 60 ) * 30,
	                beforeSend: function() {
	                	btnSubmit.attr('disabled', true);
                		jQuery(divLoading).show();
	                },
	    			success: function(data) {
	    				btnSubmit.attr('disabled', true);
	    				switch( data.status ) {
	    					case true:
	    						window.location.href = jQuery(form).attr('action');
	    					break;
	    					case false:
	                            if ( Object.keys( data.errors ).length ) {
	                                jQuery(form).validate().showErrors( data.errors );
	                            }

	                            if ( data.message ) {
	    						  jQuery('.formErrorBox').removeClass('hide').find('.textMessage').html( data.message );
	                            }
	    					default:
	    						btnSubmit.removeAttr('disabled');
	    						jQuery(divProgressBar).hide();
	    						jQuery(divLoading).hide();
	    					break;
	    				}
	    			}
	    		});
	        }
	    });
	}
});