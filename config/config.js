var	path = require('path'),
	rootPath = path.normalize(__dirname + '/..'),
	sessionConfig = {
		cookieName: 'animoutSession',
		secret: 'jjH@jlh52ljh!@21!j45n1p4h',
		duration: 7 * (24 * 60 * 60 * 1000), // how long the session will stay valid in ms (first number is number of days)
		activeDuration: 1000 * 60 * 5 // if expiresIn < activeDuration, the session will be extended by activeDuration milliseconds
	};

module.exports = {
	name: "Animout",
	extendedName: "Animout Animes Online",
	separator: ' - ',
	email: '',

	indexUrl: "/",
	ajaxurl: "/ajax",

	tempDir: path.normalize(__dirname + '/../../tmp/'),
	ftpVideos: path.normalize(__dirname + '/../../ftp_videos/'),

	uploadDirName: 'upload',

	uploadDir: path.normalize(__dirname + '/../public/upload/'),
	coverDirName: 'cover',

	storageUploadDir: path.normalize(__dirname + '/../public/upload/'),

	storageTorrentDirName: 'torrent',
	storageVideoPreEncodeDirName: 'pre-encoder',
	storageVideoDirName: 'webseed',

	cssDir: "css",
	jsDir: "js",
	imgDir: "images",
	fontDir: "fonts",

	defaultSocialImage: 'SocialImage.jpg',
	defaultLoadingVideoImage: 'video-loading.png',

	xSmallList: {
		perPage: 8
	},

	smallList: {
		perPage: 12,
	},

	mediumList: {
		perPage: 16,
	},

	bigList: {
		perPage: 20
	},

	xBigList: {
		perPage: 24
	},

	xTremeBigList: {
		perPage: 1000
	},

	ads: {
		show: {
			popunder: true,
			midroll: true,
			sliderin: true,
			preroll: true,
			lightbox: false,
			blocks: false,
			interestial: false,
		}
	},

	stickyAlert: false,

	cache: {
		maxAge: 2 * 60
	},

	recaptcha: {
		site_key: '6LeuFBoTAAAAAPwYkhVBN8N-0tMeWDf92UfnIUMB',
		secret_key: '6LeuFBoTAAAAAKew-nkbEHMHuNT1OhFx2PCWw_Vn',
		url: 'https://www.google.com/recaptcha/api/siteverify',
		host: 'www.google.com',
		path: '/recaptcha/api/siteverify'
	},

	facebook: {
		pageUrl: 'https://www.facebook.com/Animoutcombr-1073852869356821',
		appId: '1131434130229476',
		moderators: [
			//'100000636216713' // Diogo Bruni
		],
		autolike: false
	},

	notifications: {
		name: 'Animout Push',
		shortname: 'AnimoutPush',
		google: {
			id: 'animout-1291',
			number: '1076908528455',
			server_key: 'AIzaSyCtc9F5gpPi-PhzsGhU1Y34SqKxMKXV0c0'
		},
		one_signal: {
			app_id: '0e10f525-835e-4c71-b81a-4b79c8fb002f',
			rest_key: 'NGE2ZGU0MjItY2ViZi00NmI0LTg0YzAtNjI5M2FjZmEyMjc2'
		}
	},

	production: {
		root: rootPath,

		mongo: {
			url: 'mongodb://localhost/animout'
		},

		url: 'https://animout.com.br',
		commentBaseUrl: 'http://animout.com.br',
		storageUrl: 'https://upload.animout.com.br',

		sessionConfig: sessionConfig
	},
	development: {
		root: rootPath,
		
		mongo: {
			url: 'mongodb://localhost/dev_animout'
		},

		url: 'http://localhost:3002',
		commentBaseUrl: 'http://localhost:3002',
		storageUrl: 'http://localhost:3002/upload',

		sessionConfig: sessionConfig
	}
}
