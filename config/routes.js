var config = require("../config/config"),
	ajaxManager = require("../controllers/ajaxManager"),
	front = {
		controller: require("../controllers/front"),
		config: require("../config/front")
	},
	admin = {
		controller: require("../controllers/admin"),
		config: require("../config/admin")
	};

module.exports = function(app) {
	/*
	Object.keys(front.config.pages).forEach(function(key) {
		var page = front.config.pages[key];
		app.get(page.url, page.controller);
	});
	*/

	// Front
	app.get(front.config.pages.index.url, front.controller.index);
	app.get(front.config.pages.login.url, front.controller.login);
	//app.get(front.config.pages.signup.url, front.controller.signup);
	
	app.get(front.config.pages.animes.routeUrl, front.controller.animeList);
	app.get(front.config.pages.anime.routeUrl, front.controller.animeSingle);
	app.get(front.config.pages.animeRandom.url, front.controller.animeRandom);
	app.get(front.config.pages.popularEpisodes.routeUrl, front.controller.popularEpisodes);
	app.get(front.config.pages.highlightedEpisodes.routeUrl, front.controller.highlightedEpisodes);
	app.get(front.config.pages.episode.routeUrl, front.controller.episode);

	app.get(front.config.pages.searchEpisode.routeUrl, front.controller.searchEpisode);

	app.get(front.config.pages.doar.url, front.controller.doar);
	app.get(front.config.pages.adAlternative.url, front.controller.adAlternative);
	
	app.get(front.config.pages.notificationManifest.url, front.controller.notificationManifest);
	app.get(front.config.pages.notificationWorker.url, front.controller.notificationWorker);
	app.get(front.config.pages.notificationWorkerUpdate.url, front.controller.notificationWorker);
	app.get(front.config.pages.sitemap.url, front.controller.sitemap);

	// Admin
	app.get(admin.config.pages.dashboard.url, admin.controller.dashboard);

	app.get(admin.config.pages.serialNew.url, admin.controller.serialNewEdit);
	app.get(admin.config.pages.serialEdit.url, admin.controller.serialNewEdit);
	app.get(admin.config.pages.serialList.url, admin.controller.serialList);

	app.get(admin.config.pages.episodeNew.url, admin.controller.episodeNewEdit);
	app.get(admin.config.pages.episodeEdit.url, admin.controller.episodeNewEdit);
	app.get(admin.config.pages.episodeList.url, admin.controller.episodeList);

	app.get(admin.config.pages.userNew.url, admin.controller.userNewEdit);
	app.get(admin.config.pages.userEdit.url, admin.controller.userNewEdit);
	app.get(admin.config.pages.userList.url, admin.controller.userList);
	app.get(admin.config.pages.lackOfPermission.url, admin.controller.lackOfPermission);
	
	// Ajax
	app.post(config.ajaxurl, ajaxManager.init);

	// 404
	app.get(front.config.pages.notFound.url, front.controller.notFound);
}