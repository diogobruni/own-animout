var levels = {
	dashboard: {
		view: 1
	},
	user: {
		view: 5,
		add: 5,
		edit: 5,
		delete: 5
	},
	serial: {
		view: 1,
		add: 1,
		edit: 1,
		delete: 2
	},
	episode: {
		view: 1,
		add: 1,
		edit: 1,
		delete: 2
	},
	lackOfPermission: {
		view: 0
	}
};

var pages = {
	dashboard: {
		url: "/admin",
		viewPath: "admin/dashboard",
		title: "Painel Admin",
		requiredLevel: levels.dashboard
	},
	userNew: {
		url: "/admin/user-new",
		viewPath: "admin/user-new-edit",
		title: "Novo Usuário",
		requiredLevel: levels.user
	},
	userEdit: {
		url: "/admin/user-edit/:email",
		viewPath: "admin/user-new-edit",
		title: "Editar Usuário",
		requiredLevel: levels.user
	},
	userList: {
		url: "/admin/user-list",
		viewPath: "admin/user-list",
		title: "Lista de Usuários",
		requiredLevel: levels.user
	},
	serialNew: {
		url: "/admin/serial-new",
		viewPath: "admin/serial-new-edit",
		title: "Nova Série",
		requiredLevel: levels.serial
	},
	serialEdit: {
		url: "/admin/serial-edit/:slug",
		viewPath: "admin/serial-new-edit",
		title: "Editar Título",
		requiredLevel: levels.serial
	},
	serialList: {
		url: "/admin/serial-list",
		viewPath: "admin/serial-list",
		title: "Lista de Séries",
		requiredLevel: levels.serial
	},
	episodeNew: {
		url: "/admin/episode-new",
		viewPath: "admin/episode-new-edit",
		title: "Novo Episódio",
		requiredLevel: levels.episode
	},
	episodeEdit: {
		url: "/admin/episode-edit/:slug",
		viewPath: "admin/episode-new-edit",
		title: "Editar Episódio",
		requiredLevel: levels.episode
	},
	episodeList: {
		url: "/admin/episode-list",
		viewPath: "admin/episode-list",
		title: "Lista de Episódios",
		requiredLevel: levels.episode
	},
	lackOfPermission: {
		url: "/admin/lack-permission",
		viewPath: "admin/lack-permission",
		title: "Acesso Negado",
		requiredLevel: levels.lackOfPermission
	}
};

module.exports = {
	pages: pages,
	requiredLevels: levels,

	dependencyStyles: [
		{ "href": "https://fonts.googleapis.com/css?family=Roboto:400,300,500", absolutUrl: true },
		{ "href": "dependencies-admin.css" },
	],
	defaultStyles: [
		{ "href": "admin.css" },
	],

	dependencyScripts: [
		{ "src": "dependencies-admin.js" },
	],
	defaultScripts: [
		{ "src": "admin.js" },
	],

	ajaxScripts: [],
	sidebarMenu: [
		[
			{
				"ico" : "fa fa-plus",
				"page" : pages.userNew,
				"title" : pages.userNew.title,
				"href" : pages.userNew.url
			},
			{
				"ico" : "fa fa-users",
				"page" : pages.userList,
				"title" : pages.userList.title,
				"href" : pages.userList.url
			}
		],
		[
			{
				"ico" : "fa fa-plus",
				"page" : pages.serialNew,
				"title" : pages.serialNew.title,
				"href" : pages.serialNew.url
			},
			{
				"ico" : "fa fa-list",
				"page" : pages.serialList,
				"title" : pages.serialList.title,
				"href" : pages.serialList.url
			}
		],
		[
			{
				"ico" : "fa fa-plus",
				"page" : pages.episodeNew,
				"title" : pages.episodeNew.title,
				"href" : pages.episodeNew.url
			},
			{
				"ico" : "fa fa-list",
				"page" : pages.episodeList,
				"title" : pages.episodeList.title,
				"href" : pages.episodeList.url
			}
		]
	],

	genres: [
		///* 0 */ "Nenhum",
		/* 1 */ "Ação",
		/* 2 */ "Aventura",
		/* 3 */ "Animação",
		/* 4 */ "Bishounen",
		/* 5 */ "Comédia",
		/* 6 */ "Demônios/Akuma",
		/* 7 */ "Drama",
		/* 8 */ "Ecchi",
		/* 9 */ "Fantasia",
		/* 10 */ "Jogos",
		/* 11 */ "Harem",
		/* 12 */ "Hentai",
		/* 13 */ "Histórico",
		/* 14 */ "Horror",
		/* 15 */ "Josei",
		/* 16 */ "Infantil",
		/* 17 */ "Românce",
		/* 18 */ "Magia",
		/* 19 */ "Artes Marciais",
		/* 20 */ "Mecha",
		/* 21 */ "Militar",
		/* 22 */ "Musical",
		/* 23 */ "Mistério",
		/* 24 */ "Policial",
		/* 25 */ "Psicológico",
		/* 26 */ "Samurai",
		/* 27 */ "Escolar",
		/* 28 */ "Sci-Fi",
		/* 29 */ "Seinen",
		/* 30 */ "Shoujo",
		/* 31 */ "Shoujo-ai",
		/* 32 */ "Shounen",
		/* 33 */ "Shounen-ai",
		/* 34 */ "Espacial",
		/* 35 */ "Esportes",
		/* 36 */ "Super Poderes",
		/* 37 */ "Sobrenatural",
		/* 38 */ "Vampiros",
		/* 39 */ "Yaoi",
		/* 40 */ "Yuri",
		/* 41 */ "Suspense",
		/* 42 */ "Slice of Life",
		/* 43 */ "Terror"
	],

	levels: [
		/* 0 */ 'Normal',
		/* 1 */ 'Uploader',
		/* 2 */ 'Gerente',
		/* 3 */ '',
		/* 4 */ '',
		/* 5 */ 'Super Admin'
	]
}