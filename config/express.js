var sessions = require("client-sessions"),
	cookie = require('cookie-parser'),
	bodyParser = require('body-parser'),
	compression = require('compression'),
	favicon = require('serve-favicon'),
	//timeout = require('connect-timeout'),
	express = require('express');

var env = process.env.NODE_ENV || 'development';

module.exports = function (app, config) {

	if ( env == 'development' ) {
		// Enable Cors
		app.use(function(req, res, next) {
			res.header("Access-Control-Allow-Origin", "*");
			res.header("Access-Control-Allow-Headers", "Range, Origin, X-Requested-With, Content-Type, Accept");
			next();
		});
	}

	app.set('showStackError', true)

	// should be placed before express.static
	app.use(compression())

	app.use(favicon(config.root + '/public/images/favicon.ico'));
	app.use(express.static(config.root + '/public', { maxAge: 60 * 60 * 24 * 7}));

	// set views path, template engine and default layout
	app.set('views', config.root + '/views')
	app.set('view engine', 'ejs')

	//app.use(timeout('1s'));

    app.use(bodyParser.json()); // to support JSON-encoded bodies
	app.use(bodyParser.urlencoded({ extended: true })); // to support URL-encoded bodies
	app.use(sessions(config.sessionConfig));

	/*
	app.use(haltOnTimedout);
	function haltOnTimedout(req, res, next) {
		console.log(req.timedout);
		if (req.timedout) {
			res.sendStatus(408);
		} else {
			next();
		}
	}
	*/
	

	// development env config
	if ('development' == env) {
		app.locals.pretty = true
	}
}
