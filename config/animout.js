module.exports = {
	episode: {
		file: {
			defaultExtension: '.mp4'
		},
		notification: {
			title: "%s",
			message: "Clique aqui e seja o primeiro a assistir!"
		},
		minViewsHighlight: 25,
		types: [
			{ title: 'Normal', slug: 'normal', default: true },
			{ title: 'Filler', slug: 'filler', default: false },
			{ title: 'OVA', slug: 'ova', default: false },
			{ title: 'Filme', slug: 'movie', default: false },
			{ title: 'Teaser', slug: 'teaser', default: false }
		],
		typeMatches: {
			ova: [ 'ova', 'special', 'especial' ],
			movie: [ 'movie', 'filme' ],
			teaser: [ 'teaser ']
		}
	}
}