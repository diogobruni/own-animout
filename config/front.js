var pages = {
	index: {
		url: "/",
		viewPath: "front/index-thumbnail",
		title: "Animes Online",
		description: "Animout Animes Online - Assista animes online, Naruto Shippuden, One Punch Man, Dragon Ball, One Piece e muito mais",
		//route: routes.index
		changefreq: 'hourly',
		priority: 1.0
	},
	login: {
		url: "/login",
		viewPath: "front/login",
		title: "Login",
	},
	/*signup: {
		url: "/signup",
		viewPath: "front/signup",
		title: "Cadastre-se",
	},*/

	animes: {
		url: "/animes",
		urlPaged: "/animes/%s",
		routeUrl: "/animes/:page?",
		viewPath: "front/anime-list",
		title: "Lista de animes",
		description: "Lista de todos os animes online disponíveis no site Animout",
		changefreq: 'daily',
		priority: 0.8
	},

	anime: {
		url: "/anime/%s",
		urlPaged: "/anime/%s/%s",
		routeUrl: "/anime/:slugAnime/:page?",
		viewPath: "front/anime-single",
		linkTitle: "Assista %s online",
		title: "%s",
		description: "%s",
		changefreq: 'weekly',
		priority: 0.6
	},

	animeRandom: {
		url: "/anime-aleatorio"
	},

	highlightedEpisodes: {
		url: "/episodios-em-destaque",
		urlPaged: "/episodios-em-destaque/%s",
		routeUrl: "/episodios-em-destaque/:page?",
		viewPath: "front/highlighted-episode-list",
		title: "Episódios em destaque",
		description: "Lista dos episódios de animes online em destaque no site Animout",
		changefreq: 'daily',
		priority: 0.8
	},

	popularEpisodes: {
		url: "/episodios-populares",
		urlPaged: "/episodios-populares/%s",
		routeUrl: "/episodios-populares/:page?",
		viewPath: "front/popular-episode-list",
		title: "Episódios mais populares",
		description: "Lista dos episódios de animes online mais vistos no site Animout",
		changefreq: 'daily',
		priority: 0.8
	},

	episode: {
		url: "/episodio/%s/%s",
		routeUrl: "/episodio/:slugAnime/:slugEpisode",
		viewPath: "front/episode",
		title: "%s",
		linkTitle: "Assista %s online",
		description: "Assistir anime online %s legendado em português",
		changefreq: 'monthly',
		priority: 0.6
	},

	searchAnime: {
		url: "/busca/anime/%s",
		routeUrl: "/busca/anime/:page?",
		viewPath: "front/search-anime",
		title: "Buscando animes com \"%s\"",
		description: "Encontre seus animes favoritos em %s"
	},

	doar: {
		url: "/doar",
		viewPath: "front/doar",
		title: "Ajude-nos a menter o site funcionando",
		description: "Ajude-nos a manter o site funcionando, pode ser uma doação de qualquer valor"
	},

	searchEpisode: {
		url: "/busca/episodio/%s",
		urlPaged: "/busca/episodio/%s?q=%s",
		routeUrl: "/busca/episodio/:page?",
		viewPath: "front/search-episode",
		title: "Buscando episódios de anime com \"%s\"",
		description: "Encontre seus animes favoritos em %s"
	},

	adAlternative: {
		url: "/ad-alternative",
		viewPath: "front/ad-alternative",
		title: "Anúncio alternativo ao Adsense",
		description: "Anúncio alternativo ao Adsense"
	},	

	notFound: {
		url: "*",
		viewPath: "front/404",
		title: "Página não encontrada",
		description: "A página que você está procurando não existe ou não se encontra mais por aqui.",
		changefreq: 'monthly',
		priority: 0.5
	},

	sitemap: {
		url: "/sitemap.xml"
	},
	notificationManifest: {
		url: "/notification-manifest.json",
		viewPath: "front/notification-manifest"
	},
	notificationWorker: {
		url: '/OneSignalSDKWorker.js',
		viewPath: 'front/notification-worker'
	},
	notificationWorkerUpdate: {
		url: '/OneSignalSDKUpdaterWorker.js',
		viewPath: 'front/notification-worker'
	}
};
module.exports = {
	pages: pages,

	dependencyStyles: [
		{ "href": "https://fonts.googleapis.com/css?family=Roboto:400,300,500", absolutUrl: true },
		{ "href": "dependencies.css" },
	],
	defaultStyles: [
		{ "href": "front.css" },
	],

	dependencyScripts: [
		{ "src": "dependencies.js" },
		{ "src": "https://cdn.onesignal.com/sdks/OneSignalSDK.js", absolutUrl: true, async: true },
	],
	defaultScripts: [
		{ "src": "front.js" },
	],
	
	ajaxScripts: [],
	topMenu: [
		/*
		{
			"ico": "fa fa-home",
			"title": "",
			"href": pages.index.url,
			"children": []
		},
		*/
		{
			"ico": "",
			"title": "Home",
			"page": pages.index,
			"href": pages.index.url,
			"children": []
		},
		{
			"ico": "",
			"title": "Lista de Animes",
			"page": pages.animes,
			"href": pages.animes.url,
			"children": [
				/*{
					title: "Todos",
					page: pages.animes,
					href: pages.animes.url
				},
				{
					title: "Em destaque",
					page: pages.highlightedAnimes,
					href: pages.highlightedAnimes.url
				},*/
			]
		},
		{
			"ico": "",
			"title": "Episódios",
			"page": pages.popularEpisodes,
			"href": pages.popularEpisodes.url,
			"children": [
				{
					title: "Em destaque",
					page: pages.highlightedEpisodes,
					href: pages.highlightedEpisodes.url
				},
				{
					title: "Mais populares",
					page: pages.popularEpisodes,
					href: pages.popularEpisodes.url
				},
			]
		},
		/*{
			"ico": "",
			"title": "Notícias",
			"href": "http://dragonerd.com.br",
			"target": '_BLANK',
			"children": []
		},*/
		{
			"ico": "",
			"title": "Ajude a manter o site online",
			"page": pages.doar,
			"href": pages.doar.url,
			"children": []
		},
	]
};