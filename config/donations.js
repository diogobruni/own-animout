module.exports = {
	monthDonations: [
		{
			monthName: 'Agosto/2016',
			neededValue: 300,
			donatedValue: 70
		}
	],
	neededDonationValue: 300,
	donors: [
		{ name: 'Carlos Augusto', value: 20 },
		{ name: 'Alan dos Santos de Oliveira', value: 40 },
		{ name: 'Guilherme Ferreira', value: 20 },
	]
}