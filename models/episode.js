var Mongoose = require('mongoose'),
	Schema = Mongoose.Schema;

var _ = require('lodash');

var episodeSchema = new Schema({
	_owner: { type: Schema.Types.ObjectId, ref: 'Serial', required: true },
	uploader: { type: Schema.Types.ObjectId, ref: 'User', required: true },
	pubDate: { type : Date, default: Date.now },
	title: { type: String, required: true },
	slug: { type: String, required: true },
	number: Number,
	release: { type: Boolean, default: false },
	description: { type: String },
	media: {
		torrent: String,
		file: String,
		anitubeSourceUrl: String
	},
	youtube: String,
	type: {
		normal: { type: Boolean, default: true },
		filler: { type: Boolean, default: false },
		ova: { type: Boolean, default: false },
		movie: { type: Boolean, default: false },
		teaser: { type: Boolean, default: false }
	},
	tags: { type: Array, default: [] },
	views: [{
		datetime: { type : Date, default: Date.now },
		ip: { type: String, required: true }
	}],
	status: { type: Boolean, default: true }
});

/* Control Methods */

episodeSchema.methods.test = function(callback) {
	//return this.model('Episode').find(findWhere, {}, options, callback);
	var iniDate = new Date(),
		endDate = new Date();
	
	iniDate.setDate( endDate.getDate() - 7 );

	return this.model('Episode').aggregate([
		{
			$match: {
				"views.datetime": {
					$gte: iniDate,
					$lt: endDate
				},
				status: true
			}
		}, {
			$project: {
				_owner: 1,
				uploader: 1,
				pubDate: 1,
				title: 1,
				slug: 1,
				description: 1,
				media: 1,
				youtube: 1,
				tags: 1,
				viewsCount: { $size: "$views" },
				status: 1
			}
		},
		{
			$limit: 1
		},
		{
			$skip: 0
		},
		{
			$sort: {
				title: 1
			}
		}
	], callback);
}

episodeSchema.methods.populateThis = function(list, callback) {
	this.model('Episode').populate(list, {path: "_owner"}, callback);
}

episodeSchema.methods.checkIfExists = function(callback) {
	var where = {
		slug: this.slug
	};
	if ( this._id ) {
		where['_id'] = { $ne: this._id };
	}
	return this.model('Episode').count(where, callback);
}

episodeSchema.methods.getByOwnerPaged = function(ownerId, page, perPage, callback) {
	page--;

	var options = {
		where: {
			status: true,
			_owner: ownerId
		},
		limit: perPage,
		skip: page * perPage,
		sort: {
			number: 1
		}
	};

	var that = this;
	this.advancedAggregate(options, function(err, result) {
		that.populateThis(result, callback);
	});
}

episodeSchema.methods.getCustomPaged = function(pOptions, page, perPage, callback) {
	page--;

	var options = {
		where: pOptions.where,
		limit: perPage,
		skip: page * perPage,
		sort: {
			number: 1
		}
	};

	var that = this;
	this.advancedAggregate(options, function(err, result) {
		that.populateThis(result, callback);
	});
}

episodeSchema.methods.count = function(callback) {
	var findWhere = {
			status: true,
		};
	return this.model('Episode').count(findWhere, callback);
}

episodeSchema.methods.countByOwner = function(ownerId, callback) {
	var findWhere = {
			status: true,
			_owner: ownerId
		};
	return this.model('Episode').count(findWhere, callback);
}

episodeSchema.methods.countByHighlighted = function(callback) {
	var animoutConfig = require('../config/animout');
	var findWhere = {
			status: true,
		};
	findWhere[ 'views.' + animoutConfig.episode.minViewsHighlight ] = { $exists: true };
	return this.model('Episode').count(findWhere, callback);
}

episodeSchema.methods.countBySearch = function(searchQuery, callback) {
	var findWhere = {
			title: new RegExp(searchQuery, 'i'),
			status: true
		};
	return this.model('Episode').count(findWhere, callback);
};

episodeSchema.methods.insert = function(callback) {
	var sReturn = { status: true, errors: {} };

	if ( this.title && this.slug ) {
		this.save(function(err, createdRegistry) {
			if (err) {
				//console.dir(handleError(err));
				
				sReturn.status = false;
				sReturn.errors.global = "Ops, houve um erro.";
			}

			callback( sReturn );
		});
	} else {
		
		callback( sReturn );
	}
}

episodeSchema.methods.update = function(callback) {
	var sReturn = { status: true, errors: {} };

	var newObj = this.toObject();
	delete newObj.pubDate;
	delete newObj.views;

	this.model('Episode').findByIdAndUpdate( this._id, newObj, function( err, result ) {
		if ( err ) {
			//console.log( err );
			sReturn.status = false;
			sReturn.errors.global = "Ops, houve um erro.";
		} else {
			callback( sReturn );
		}
	});
}

episodeSchema.methods.pushView = function(id, view, callback) {
	var sReturn = { status: true, errors: {} };
	var options = {
		//upsert: true
	};

	this.model('Episode').findByIdAndUpdate(id, view, options, function(err, result) {
		if (err) {
			sReturn.status = false;
		} else {
			callback(sReturn);
		}
	});
}

episodeSchema.methods.getBySlugAddView = function(slug, view, callback) {
	var rError = false;
	var options = {
		//upsert: true
	};

	var that = this;

	var options = {
			where: {
				slug: slug
			},
			limit: 1
		};

	this.advancedAggregate(options, function(err, rEpisode) {
		if (err || !rEpisode.length) {
			rError = true;
			return callback(rError);
		}
		that.model('Episode').findByIdAndUpdate(rEpisode[0]._id, view, {}, function(err, result) {
			if (err) {
				rError = true;
				return callback(rError);
			} else {
				that.populateThis(rEpisode, callback);
			}
		});
	});
}

/*
episodeSchema.methods.update = function(callback) {
	var sReturn = { status: true, errors: {} };

	var newThis = this;
	this.model('Episode').findOne({ _id: this._id }, function(err, result) {
		if ( !err ) {
			result = newThis;
			result.save(function( err ) {
				if ( err ) {
					sReturn.status = false;
					console.log( err );
				}
				callback( sReturn );
			});
		}
	});
}
*/

episodeSchema.methods.getAll = function(callback) {
	return this.model('Episode').find({ status: true }, callback);
}

episodeSchema.methods.getById = function(id, callback) {
	var findWhere = { _id: id };
	return this.model('Episode').findOne(findWhere, callback);
}

episodeSchema.methods.getBySlug = function(slug, callback) {
	var findWhere = { slug: slug };
	//return this.model('Episode').findOne(findWhere, callback);
	return this.model('Episode').findOne(findWhere).populate('_owner').exec(callback);
}

episodeSchema.methods.getAllOrdererByTitle = function(callback) {
	var options = {
		sort: { title: 'asc'}
	};
	return this.model('Episode').find({}, null, options, callback);
}

episodeSchema.methods.getCustom = function(extraWhere, extraOptions, callback) {
	var defaultWhereParams = {};
	var findWhere = _.extend(defaultWhereParams, extraWhere);

	var defaultOptionsParams = {
		sort: { _id: -1 }
	};
	var findOptions = _.extend(defaultOptionsParams, extraOptions);

	return this.model('Episode').find(findWhere, null, findOptions, callback);
}

episodeSchema.methods.getAllOrdererByLast = function(callback) {
	var options = {
		sort: { _id: -1}
	};
	return this.model('Episode').find({}, null, options, callback);
}

episodeSchema.methods.advancedAggregate = function(options, callback) {

	var aggregateParam = [];

	aggregateParam.push({
		$project: {
			_owner: 1,
			uploader: 1,
			pubDate: 1,
			title: 1,
			number: 1,
			release: 1,
			slug: 1,
			description: 1,
			media: 1,
			type: 1,
			youtube: 1,
			tags: 1,
			viewsCount: { $size: "$views" },
			status: 1
		}
	});

	var where = options.where || options.match;
	if ( where ) {
		aggregateParam.push({ $match: where });
	}

	var sort = options.sort;
	if ( sort ) {
		aggregateParam.push({ $sort: sort });
	}
	
	var skip = options.skip;
	if ( skip ) {
		aggregateParam.push({ $skip: skip });
	}

	var limit = options.limit;
	if ( limit ) {
		aggregateParam.push({ $limit: limit });
	}

	return this.model('Episode').aggregate(aggregateParam, callback);
}

// Custom List Methods
episodeSchema.methods.aggregateAllOrderedByTitle = function(callback) {
	var options = {
		where: {
			status: true
		},
		sort: {
			title: 1
		}
	};

	var that = this;
	this.advancedAggregate(options, function(err, result) {
		that.populateThis(result, callback);
	});
}

episodeSchema.methods.getSortedByViewsPaged = function(page, perPage, callback) {
	page--;

	var options = {
		where: {
			status: true
		},
		limit: perPage,
		skip: page * perPage,
		sort: {
			viewsCount: -1,
			pubDate: 1
		}
	};

	var that = this;
	this.advancedAggregate(options, function(err, result) {
		that.populateThis(result, callback);
	});
}

episodeSchema.methods.getHighlightedPaged = function(page, perPage, callback) {
	page--;

	var animoutConfig = require('../config/animout');
	var options = {
		where: {
			viewsCount: {
				$gte: animoutConfig.episode.minViewsHighlight
			},
			status: true
		},
		limit: perPage,
		skip: page * perPage,
		sort: {
			pubDate: -1
		}
	};

	var that = this;
	this.advancedAggregate(options, function(err, result) {
		that.populateThis(result, callback);
	});
}

episodeSchema.methods.getReleasesPaged = function(page, perPage, callback) {
	page--;

	var animoutConfig = require('../config/animout');
	var options = {
		where: {
			release: true,
			status: true
		},
		limit: perPage,
		skip: page * perPage,
		sort: {
			pubDate: -1
		}
	};

	var that = this;
	this.advancedAggregate(options, function(err, result) {
		that.populateThis(result, callback);
	});
}

episodeSchema.methods.getWeekPopularPaged = function(page, perPage, callback) {
	page--;

	var iniDate = new Date(),
		endDate = new Date();
	
	iniDate.setDate( endDate.getDate() - 7 );

	var options = {
		where: {
			"views.datetime": {
				$gte: iniDate,
				$lt: endDate
			},
			status: true
		},
		limit: perPage,
		skip: page * perPage,
		sort: {
			viewsCount: -1,
			pubDate: 1
		}
	};

	var that = this;
	this.advancedAggregate(options, function(err, result) {
		that.populateThis(result, callback);
	});
}

episodeSchema.methods.getRecentsPaged = function(page, perPage, callback) {
	page--;

	var options = {
		where: {
			status: true
		},
		limit: perPage,
		skip: page * perPage,
		sort: {
			pubDate: -1
		}
	};

	var that = this;
	this.advancedAggregate(options, function(err, result) {
		that.populateThis(result, callback);
	});
}

episodeSchema.methods.searchByTitlePaged = function(page, perPage, searchQuery, callback) {
	page--;

	var options = {
		where: {
			title: new RegExp(searchQuery, 'i'),
			status: true
		},
		limit: perPage,
		skip: page * perPage,
		sort: {
			title: 1
		}
	};

	var that = this;
	this.advancedAggregate(options, function(err, result) {
		that.populateThis(result, callback);
	});
};

episodeSchema.methods.getNext = function(objEpisode, limit, callback) {
	var options = {
		where: {
			_owner: objEpisode._owner._id,
			//_id: { $gt: objEpisode._id }
			number: { $gt: objEpisode.number }
		},
		limit: limit,
		sort: {
			number: 1
		}
	};

	if ( objEpisode.type.normal || objEpisode.type.filler ) {
		options.where['$or'] = [
			{ "type.normal": true },
			{ "type.filler": true }
		];
	} else if ( objEpisode.type.ova ) {
		options.where["type.ova"] = true;
	} else if ( objEpisode.type.movie ) {
		options.where["type.movie"] = true;
	} else if ( objEpisode.type.teaser ) {
		options.where["type.teaser"] = true;
	}

	var that = this;
	this.advancedAggregate(options, function(err, result) {
		that.populateThis(result, callback);
	});
}

episodeSchema.methods.getPrev = function(objEpisode, limit, callback) {
	var options = {
		where: {
			_owner: objEpisode._owner._id,
			//_id: { $lt: objEpisode._id }
			number: { $lt: objEpisode.number }
		},
		limit: limit,
		sort: {
			number: -1
		}
	};

	if ( objEpisode.type.normal || objEpisode.type.filler ) {
		options.where['$or'] = [
			{ "type.normal": true },
			{ "type.filler": true }
		];
	} else if ( objEpisode.type.ova ) {
		options.where["type.ova"] = true;
	} else if ( objEpisode.type.movie ) {
		options.where["type.movie"] = true;
	} else if ( objEpisode.type.teaser ) {
		options.where["type.teaser"] = true;
	}


	var that = this;
	this.advancedAggregate(options, function(err, result) {
		that.populateThis(result, callback);
	});
}

Mongoose.model('Episode', episodeSchema);