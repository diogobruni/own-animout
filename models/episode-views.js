var Mongoose = require('mongoose'),
	Schema = Mongoose.Schema;

var episodeViewSchema = new Schema({
	_owner: { type: Schema.Types.ObjectId, ref: 'Episode', required: true },
	datetime: { type : Date, default: Date.now },
	ip: { type: String, required: true }
});

/* Control Methods */

episodeViewSchema.methods.insert = function(callback) {
	this.save(function(err, createdRegistry) {
		if (err) return handleError(err);

		callback( this );
	});
}

episodeViewSchema.methods.countByOwner = function(ownerId, callback) {
	var findWhere = {
		_owner: ownerId
	};
	return this.model('EpisodeViews').count(findWhere, callback);
}

episodeViewSchema.methods.countByOwnerDatetimeRange = function(ownerId, iniDatetime, endDatetime, callback) {
	var findWhere = {
		_owner: ownerId,
		datetime: {
			"$gte": iniDatetime,
			"$lt": endDatetime
		}
	};
	return this.model('EpisodeViews').count(findWhere, callback);
}

Mongoose.model('EpisodeViews', episodeViewSchema);
