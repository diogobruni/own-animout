var Mongoose = require('mongoose'),
	Schema = Mongoose.Schema;

var serialSchema = new Schema({
	title: { type: String, required: true },
	slug: { type: String, required: true, index: { unique: true } },
	cover: { type: String },
	genre: { type: Array },
	synopsis: { type: String },
	status: { type: Boolean, required: true, default: true },
});

/* Control Methods */

serialSchema.methods.checkIfExists = function(callback) {
	var where = {
		slug: this.slug
	};
	if ( this._id ) {
		where['_id'] = { $ne: this._id };
	}
	return this.model('Serial').count(where, callback);
}

serialSchema.methods.insert = function(callback) {
	var sReturn = { status: true, errors: {} };

	if ( this.title && this.slug ) {
		this.save(function(err, createdRegistry) {
			if (err) {
				//console.dir(handleError(err));
				
				sReturn.status = false;
				sReturn.errors.global = "Ops, houve um erro.";
			}

			callback( sReturn );
		});
	} else {
		
		callback( sReturn );
	}
}

serialSchema.methods.update = function(callback) {
	var sReturn = { status: true, errors: {} };

	this.model('Serial').findByIdAndUpdate( this._id, this, function( err, result ) {
		if ( err ) {
			console.log( err );
			sReturn.status = false;
			sReturn.errors.global = "Ops, houve um erro.";
		} else {
			callback( sReturn );
		}
	});
}

serialSchema.methods.count = function(callback) {
	var findWhere = {
		status: true
	};
	return this.model('Serial').count(findWhere, callback);
}

serialSchema.methods.getRandom = function(callback) {
	var findWhere = {
		status: true
	};
	this.model('Serial').count(findWhere, function(err, rCount) {
		if (err) {
			callback(err);
		} else {
			var rand = Math.floor(Math.random() * rCount);
			this.findOne().skip(rand).exec(callback);
		}
	});
}

serialSchema.methods.getAll = function(callback) {
	var findWhere = {
		status: true
	};
	var options = {
		sort: { title: 1 }
	};
	return this.model('Serial').find(findWhere, null, options, callback);
}

serialSchema.methods.getById = function(id, callback) {
	var findWhere = {
		_id: id,
		status: true
	};
	return this.model('Serial').findOne(findWhere, callback);
}

serialSchema.methods.getBySlug = function(slug, callback) {
	var findWhere = {
		slug: slug,
		status: true
	};
	return this.model('Serial').findOne(findWhere, callback);
}

serialSchema.methods.getAllOrdererByTitle = function(page, perPage, callback) {
	if ( typeof(page) == 'function' ) {
		callback = page;
		page = 1;
		perPage = 24;
	}

	page--;

	var findWhere = {
		status: true
	};

	var options = {
		sort: { title: 'asc' },
		limit: perPage,
		skip: page * perPage
	};
	return this.model('Serial').find(findWhere, null, options, callback);
}

serialSchema.methods.getAllOrdererByLast = function(page, perPage, callback) {
	if ( typeof(page) == 'function' ) {
		callback = page;
		page = 1;
		perPage = 24;
	}

	page--;

	var findWhere = {
		status: true
	};

	var options = {
		sort: { _id: -1 },
		limit: perPage,
		skip: page * perPage
	};
	return this.model('Serial').find(findWhere, null, options, callback);
}

serialSchema.methods.searchByTitle = function(title, page, perPage, callback) {
	page--;

	var findWhere = {
		title: new RegExp(title, 'i'),
		status: true
	};

	var options = {
		sort: { title: 'asc' },
		limit: perPage,
		skip: page * perPage
	}

	return this.model('Serial').find(findWhere, { _id: 'id', title: 'name' }, options, callback);
}

serialSchema.methods.searchByTitlePaged = function(page, perPage, searchQuery, callback) {
	page--;

	var findWhere = {
		title: new RegExp(searchQuery, 'i'),
		status: true
	};

	var options = {
		//sort: { pubDate: 'desc' },
		limit: perPage,
		skip: page * perPage
	};
	return this.model('Serial').find(findWhere, null, options, callback);
};

serialSchema.methods.getByGenres = function(arSerial, limit, callback) {
	var aggregateParam = [];

	aggregateParam.push({ $match: { genre: { $in: arSerial.genre }, _id: { $ne: arSerial._id }, status: true } });
	aggregateParam.push({ $unwind: "$genre" });
	aggregateParam.push({ $match: { genre: { $in: arSerial.genre }, _id: { $ne: arSerial._id }, status: true } });
	aggregateParam.push({
		$group: {
			_id: {
				_id: '$_id',
				title: '$title',
				slug: '$slug',
				cover: '$cover',
				//genre: '$genre', // Cant use this
				// synopsis: '$synopsis' // Dont need this
			},
			matches: { $sum: 1 }
		}
	});
	aggregateParam.push({ $sort: { matches: -1 } });
	aggregateParam.push({ $limit: limit });

	return this.model('Serial').aggregate(aggregateParam, callback);
}

Mongoose.model('Serial', serialSchema);