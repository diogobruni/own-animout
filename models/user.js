var Mongoose = require('mongoose'),
	Schema = Mongoose.Schema;

/*	Levels
	0 - Normal User
	1 - Supervisor
	2 - Gerente
	5 - Super Admin
*/
var userSchema = new Schema({
	email: { type: String, required: true, index: { unique: true } },
	password: { type: String, required: true },
	nickname: { type: String },
	description: { type: String },
	photo: { type: String },
	level: { type: Number }
});

userSchema.methods.checkExistEmail = function(callback) {
	var where = {
		email: this.email
	};
	if ( this._id ) {
		where['_id'] = { $ne: this._id };
	}

	return this.model('User').count(where, callback);
}

userSchema.methods.authenticateUser = function(callback) {
	return this.model('User').find({email: this.email, password: this.password}, callback);
}

userSchema.methods.getByEmail = function(email, callback) {
	var findWhere = {
		email: email
	};
	return this.model('User').findOne(findWhere, callback);
}

userSchema.methods.createUser = function(callback) {
	if (this.email && this.password && this.nickname && this.level) {
		this.save(function(err, createdUser) {
			if (err) {
				console.dir(handleError(err));
				return false;
			}

			return createdUser;
		});
	} else {
		console.log("Campos obrigatórios não foram preenchidos.");
		return false;
	}
}

userSchema.methods.insert = function(callback) {
	var sReturn = { status: true, errors: {} };

	if ( this.email && this.password ) {
		this.save(function(err, createdRegistry) {
			if (err) {
				//console.dir(handleError(err));
				
				sReturn.status = false;
				sReturn.errors.global = "Ops, houve um erro.";
			}

			callback( sReturn );
		});
	} else {
		sReturn.status = false;
		sReturn.errors.global = 'Campos obrigatórios não preenchidos!';
		callback( sReturn );
	}
}

userSchema.methods.update = function(callback) {
	var sReturn = { status: true, errors: {} };

	this.model('User').findByIdAndUpdate( this._id, this, function( err, result ) {
		if ( err ) {
			console.log( err );
			sReturn.status = false;
			sReturn.errors.global = "Ops, houve um erro.";
		} else {
			callback( sReturn );
		}
	});
}

userSchema.methods.getAllOrdererLevel = function(page, perPage, callback) {
	if ( typeof(page) == 'function' ) {
		callback = page;
		page = 1;
		perPage = 24;
	}

	page--;

	var findWhere = {};

	var options = {
		sort: { level: -1 },
		limit: perPage,
		skip: page * perPage
	};
	return this.model('User').find(findWhere, null, options, callback);
}

Mongoose.model('User', userSchema);