var sendNotification = function(data, callback) {
	var headers = {
		"Content-Type": "application/json",
		"Authorization": "Basic NGE2ZGU0MjItY2ViZi00NmI0LTg0YzAtNjI5M2FjZmEyMjc2"
	};

	var options = {
		host: "onesignal.com",
		port: 443,
		path: "/api/v1/notifications",
		method: "POST",
		headers: headers
	};

	var https = require('https');
	var req = https.request(options, function(res) {  
		res.on('data', function(data) {
			callback(data);
		});
	});

	req.on('error', function(e) {
		console.log("ERROR:");
		console.log(e);
	});

	//req.write(JSON.stringify(data));
	//req.end();
};

var message = { 
	app_id: "0e10f525-835e-4c71-b81a-4b79c8fb002f",
	headings: {
		"en": 'Boku no Hero Academia 04',
		"br": 'Boku no Hero Academia 04'
	},
	contents: {
		"en": "Clique aqui para assistir agora!",
		"br": "Clique aqui para assistir agora!"
	},
	url: 'https://animout.com.br/episodio/boku-no-hero-academia/boku-no-hero-academia-04',
	included_segments: ["All"]
};

sendNotification(message, function(data) {
	//console.log(data);
});