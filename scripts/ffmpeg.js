var ffmpeg = require('../fluent-ffmpeg');

/*
ffmpeg('Usakame.mkv')
.output('Usakame_node_ffmpeg.mp4')
.format('mp4')
.size('1280x720')
.videoBitrate(800)
.videoCodec('libx264')
.audioCodec('libfaac')
.audioBitrate('96')
.audioChannels(2)
.audioFrequency(24000)
.on('progress', function(progress) {
	console.log('Processing: ' + progress.percent + '% done');
})
.on('end', function(stdout, stderr) {
	console.log('Finished');
})
.on('error', function(err, stdout, stderr) {
	console.log('Cannot process video: ' + err.message);
})
.run();

/*
ffmpeg.ffprobe('./Haifuri 05-no.mkv', function(err, metadata) {
	console.log(metadata);
});
*/


var proc = ffmpeg('./Usakame.mkv')
  // use the 'podcast' preset (located in /lib/presets/podcast.js)
  .preset('podcast')
  // in case you want to override the preset's setting, just keep chaining
  .videoBitrate('512k')
  // setup event handlers
  .on('end', function() {
    console.log('file has been converted succesfully');
  })
  .on('error', function(err) {
    console.log('an error happened: ' + err.message);
  })
  // save to file
  .save('./Usakame_node_ffmpeg.mp4');