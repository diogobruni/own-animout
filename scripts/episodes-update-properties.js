require('../models/episode.js');

var env = process.env.NODE_ENV || 'development',
	config = require('../config/config')[env],
	Mongoose = require('mongoose'),
	//Schema = Mongoose.Schema.
	Episode = Mongoose.model('Episode');

var connect = function() {
	var options = { server: { socketOptions: { keepAlive: 1 } } }
	Mongoose.connect(config.mongo.url, options);
}
connect();

// Error handler
Mongoose.connection.on('error', function (err) {
	console.log(err);
});

// Reconnect when closed
Mongoose.connection.on('disconnected', function () {
	connect();
});

Mongoose.connection.once('open', function() {
	console.log('Mongo Open');
	var objEpisode = new Episode();

	var animoutConfig = require('../config/animout');

	var episodeTypes = animoutConfig.episode.types;
	var matches = animoutConfig.episode.typeMatches;

	console.log('Quering episodes');
	objEpisode.getAll(function(err, rEpisodes) {
		console.log('Query executed');
		if (err) {
			console.log('Erro na busca dos episódios!');
			return false;
		}

		console.log(rEpisodes.length + ' episódios');

		var auxI = 0,
			totalI = rEpisodes.length;

		for(i in rEpisodes) {
			var rEpisode = rEpisodes[i];

			// Set type(normal|ova|movie|teaser)
			var selectedType = 'normal';
			for( type in matches ) {
				for( key in matches[type] ) {
					var strKey = matches[type][key];
					if ( ~rEpisode.title.toLowerCase().indexOf( strKey ) ) {
						selectedType = type;
					}
				}
			}
			for( j in episodeTypes ) {
				var episodeType = episodeTypes[j];

				var boolVal = false;
				if ( episodeType.slug == selectedType ) {
					boolVal = true;
				}
				rEpisode.type[episodeType.slug] = boolVal;
			}

			// Set episode number
			var regexEpisodeNumber = /(\d+(\.\d+)?)/;
			var matchEpisodeNumber = rEpisode.title.match(regexEpisodeNumber);
			if ( matchEpisodeNumber ) {
				rEpisode.number = parseInt(matchEpisodeNumber[0]);
			} else {
				console.log('Número do episódio indefinido: ' + rEpisode.title);
				rEpisode.number = 0;
			}
			rEpisode.save(function(err) {
				if (err) {
					console.log('Erro ao atualizar');
				}
			});
			auxI++;
			if ( auxI >= totalI ) {
				console.log('Fim');
			}
		}
	});
});