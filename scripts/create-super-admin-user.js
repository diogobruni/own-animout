require('../models/user.js');

var env = process.env.NODE_ENV || 'development',
	config = require('../config/config')[env],
	Mongoose = require('mongoose'),
	//Schema = Mongoose.Schema.
	User = Mongoose.model('User');
	
var connect = function() {
	var options = { server: { socketOptions: { keepAlive: 1 } } }
	Mongoose.connect(config.mongo.url, options);
}
connect();

// Error handler
Mongoose.connection.on('error', function (err) {
	console.log(err);
});

// Reconnect when closed
Mongoose.connection.on('disconnected', function () {
	connect();
});

Mongoose.connection.once('open', function() {
	var adminUser = new User({
		email: 'diogobruni@gmail.com',
		nickname: 'Admin',
		password: 'diogob14',
		level: 5
	});
	// Verirfica se o usuário do e-mail acima existe
	adminUser.authenticateUser(function(err, user) {
		// Se não existir
		if (user.length == 0) {
			// Cria
			adminUser.createUser(function(rAdminUser) {
				// Callback
			});
		}
	});
});