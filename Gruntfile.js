module.exports = function( grunt ) {

	var globalConfig = {
		dist: 'public',
		iamges: 'images',
		sass: '_sass',
		css: 'css',
		_js: '_js',
		js: 'js',
		fonts: 'fonts',
		bower_path: 'bower_components',
		bower_front_filename: 'dependencies',
		bower_admin_filename: 'dependencies-admin'
	};

	globalConfig.uglifyFiles = [
		{ // Manual files
			// Front file
			'<%= globalConfig.dist %>/<%= globalConfig.js %>/front.js' : [
				//'<%= globalConfig.dist %>/<%= globalConfig._js %>/_ajax-navigator.js',
				'<%= globalConfig.dist %>/<%= globalConfig._js %>/_front.js',
				'<%= globalConfig.dist %>/<%= globalConfig._js %>/_episode-player.js',
				'<%= globalConfig.dist %>/<%= globalConfig._js %>/_front_push_service.js'
			],
			// Login file
			'<%= globalConfig.dist %>/<%= globalConfig.js %>/login.js' : [
				'<%= globalConfig.dist %>/<%= globalConfig._js %>/_default-validate-config.js',
				'<%= globalConfig.dist %>/<%= globalConfig._js %>/_login.js'
			],
			// Admin file
			'<%= globalConfig.dist %>/<%= globalConfig.js %>/admin.js' : [
				'<%= globalConfig.dist %>/<%= globalConfig._js %>/_default-validate-config.js',
				'<%= globalConfig.dist %>/<%= globalConfig._js %>/_admin.js'
			],
			// title file
			'<%= globalConfig.dist %>/<%= globalConfig.js %>/admin-serial.js' : [
				'<%= globalConfig.dist %>/<%= globalConfig._js %>/_admin-serial-new-edit.js',
				'<%= globalConfig.dist %>/<%= globalConfig._js %>/_admin-serial-list.js'
			],
			'<%= globalConfig.dist %>/<%= globalConfig.js %>/admin-episode.js' : [
				'<%= globalConfig.dist %>/<%= globalConfig._js %>/_admin-episode-new-edit.js',
				'<%= globalConfig.dist %>/<%= globalConfig._js %>/_admin-episode-list.js'
			],
			'<%= globalConfig.dist %>/<%= globalConfig.js %>/admin-user.js' : [
				'<%= globalConfig.dist %>/<%= globalConfig._js %>/_admin-user-new-edit.js',
				'<%= globalConfig.dist %>/<%= globalConfig._js %>/_admin-user-list.js'
			],
			'<%= globalConfig.dist %>/<%= globalConfig.js %>/admin-episode-anitube.js' : [
				'<%= globalConfig.dist %>/<%= globalConfig._js %>/_admin-episode-anitube.js'
			],
		},
		{ // Automatic Files (Ignore _*.js files)
			expand: true,
			cwd: '<%= globalConfig.dist %>/<%= globalConfig._js %>',
			src: [
				"**/*.js",
				"!**/_*.js"
			],
			dest: '<%= globalConfig.dist %>/<%= globalConfig.js %>'
		}
	];

	globalConfig.deployUglifyFiles = {
		// Bower dependencies
		'<%= globalConfig.dist %>/<%= globalConfig.js %>/<%= globalConfig.bower_front_filename %>.js' : [ '<%= globalConfig.dist %>/<%= globalConfig.js %>/<%= globalConfig.bower_front_filename %>.js' ],
		//'<%= globalConfig.dist %>/<%= globalConfig.js %>/<%= globalConfig.bower_admin_filename %>.js' : [ '<%= globalConfig.dist %>/<%= globalConfig.js %>/<%= globalConfig.bower_admin_filename %>.js' ],
	};

	/* ----------------------------------- END OF CONFIG ----------------------------------- */

	var bowerDependencies = {
		'bootstrap': 'jquery',
		'select2': 'jquery',
		'datatables': 'jquery',
		'jquery-validation': 'jquery',
		'jqueryui': 'jquery',
		'bootstrap-material-design': 'bootstrap',
		'bootbox.js': 'bootstrap',
		'bootstrap-tagsinput': 'bootstrap',
		'select2-bootstrap-theme': 'bootstrap',
		'videojs-persistvolume': 'video.js',
		'videojs-vast-vpaid': 'video.js'
	}
	var bowerMainFiles = {
		'bootstrap' : [ 'dist/js/bootstrap.min.js', 'dist/css/bootstrap.min.css' ],
		'bootstrap-material-design' : [ 'dist/css/bootstrap-material-design.min.css', 'dist/js/material.min.js', 'dist/css/ripples.min.css', 'dist/js/ripples.min.js' ],
		'font-awesome' : 'css/font-awesome.min.css',
		'select2' : [ 'dist/js/select2.min.js', 'dist/css/select2.min.css' ],
		'datatables': [ 'media/js/jquery.dataTables.js', 'media/js/dataTables.bootstrap.js' ],
		'bootstrap-tagsinput': [ 'dist/bootstrap-tagsinput.min.js', 'dist/bootstrap-tagsinput.css' ],
		'select2-bootstrap-theme': [ 'dist/select2-bootstrap.min.css' ],
		'webtorrent': ['webtorrent.min.js'],
		'videojs-vast-vpaid': [ 'bin/videojs_5.vast.vpaid.min.js', 'videojs.vast.vpaid.min.css' ]
	};

	grunt.initConfig({
		globalConfig: globalConfig,

		bower_concat: {
	    	front: {
	    		dest: {
	    			js: '<%= globalConfig.dist %>/<%= globalConfig.js %>/<%= globalConfig.bower_front_filename %>.js',
	    			css: '<%= globalConfig.dist %>/<%= globalConfig.css %>/<%= globalConfig.bower_front_filename %>.css'
	    		},
	    		include: [
	    			'jquery',
	    			'bootstrap',
	    			'bootstrap-material-design',
	    			'select2',
	    			'select2-bootstrap-theme',
	    			'font-awesome',
	    			'jquery-validation',
	    			'webtorrent',
	    			'video.js',
	    			'videojs-hotkeys',
	    			'videojs-persistvolume',
	    			'videojs-vast-vpaid'
	    		],
	    		exclude: [],
	    		dependencies: bowerDependencies,
	    		mainFiles: bowerMainFiles,
	    		bowerOptions: {
	    			relative: false
	    		}
	    	},
	    	admin: {
	    		dest: {
	    			js: '<%= globalConfig.dist %>/<%= globalConfig.js %>/<%= globalConfig.bower_admin_filename %>.js',
	    			css: '<%= globalConfig.dist %>/<%= globalConfig.css %>/<%= globalConfig.bower_admin_filename %>.css'
	    		},
	    		include: [
	    			'jquery',
	    			'bootstrap',
	    			'bootstrap-material-design',
	    			'select2',
	    			'font-awesome',
	    			'jquery-validation',
	    			'datatables',
	    			'bootbox.js',
	    			'bootstrap-tagsinput',
	    			'select2-bootstrap-theme'
	    		],
	    		exclude: [],
	    		dependencies: bowerDependencies,
	    		mainFiles: bowerMainFiles,
	    		bowerOptions: {
	    			relative: false
	    		}
	    	}
	    },

	    copy: {
			main: {
				files: [
					{
						expand: true,
						flatten: true,
						src: '<%= globalConfig.bower_path %>/font-awesome/fonts/*',
						dest: '<%= globalConfig.dist %>/<%= globalConfig.fonts %>' 
					},
					{
						expand: true,
						flatten: true,
						src: '<%= globalConfig.bower_path %>/zeroclipboard/dist/*.swf',
						dest: '<%= globalConfig.dist %>/js' 
					}
				]
			}
	    },

	    sass: {
	    	options: {
	    		outputStyle: 'compressed'
	    	},
	    	dist: {
	    		/*
	    		files: {
	    			'<%= globalConfig.dist %>/<%= globalConfig.css %>/front.css' : '<%= globalConfig.dist %>/<%= globalConfig.sass %>/front.scss',
	    			'<%= globalConfig.dist %>/<%= globalConfig.css %>/user-area.css' : '<%= globalConfig.dist %>/<%= globalConfig.sass %>/user-area.scss',
	    			'<%= globalConfig.dist %>/<%= globalConfig.css %>/admin.css' : '<%= globalConfig.dist %>/<%= globalConfig.sass %>/admin.scss'
	    		}
	    		*/
	    		files: [{
    				expand: true,
    				cwd: '<%= globalConfig.dist %>/<%= globalConfig.sass %>',
    				src: [
					  "**/*.scss",
					  "!**/_*.scss"
					],
    				dest: '<%= globalConfig.dist %>/<%= globalConfig.css %>',
    				ext: '.css'
    			}]
	    	}
	    },

		uglify : {
			options : {
				mangle : false
			},

			my_target : {
				files : globalConfig.uglifyFiles
			}
    	},

    	cssmin: {
			options: {
				shorthandCompacting: false,
				roundingPrecision: -1
			},
			target: {
				files: {
					'<%= globalConfig.dist %>/<%= globalConfig.css %>/<%= globalConfig.bower_front_filename %>.css' : [ '<%= globalConfig.dist %>/<%= globalConfig.css %>/<%= globalConfig.bower_front_filename %>.css' ],
					'<%= globalConfig.dist %>/<%= globalConfig.css %>/<%= globalConfig.bower_admin_filename %>.css' : [ '<%= globalConfig.dist %>/<%= globalConfig.css %>/<%= globalConfig.bower_admin_filename %>.css' ]
				}
			}
		},

	    watch : {
	    	dist : {
	    		files : [
	    			//'<%= globalConfig.bower_path %>/**/*',
		    		'<%= globalConfig.dist %>/<%= globalConfig._js %>/**/*',
		    		'<%= globalConfig.dist %>/<%= globalConfig.sass %>/**/*'
	    		],

	    		tasks : [ 'bower_concat', 'copy', 'sass', 'uglify', 'cssmin' ]
	    	}
    	}

	});

	// Plugins do Grunt
	grunt.loadNpmTasks( 'grunt-bower-concat' );
	grunt.loadNpmTasks( 'grunt-sass' );
	grunt.loadNpmTasks( 'grunt-contrib-copy' );
	grunt.loadNpmTasks( 'grunt-contrib-uglify' );
	grunt.loadNpmTasks( 'grunt-contrib-cssmin' );
	grunt.loadNpmTasks( 'grunt-contrib-watch' );

	// Tarefas que serão executadas
	grunt.registerTask( 'default', [ 'bower_concat', 'copy', 'sass', 'uglify', 'cssmin' ] );

	grunt.registerTask( 'deploy', 'Deploy function', function() {
		globalConfig.uglifyFiles.push( globalConfig.deployUglifyFiles );
		grunt.task.run([ 'bower_concat', 'copy', 'sass', 'uglify', 'cssmin' ]);
	});

	// Tarefas para Watch
	grunt.registerTask( 'w', [ 'watch' ] );

};