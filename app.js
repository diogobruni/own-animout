// Module Dependecies
var express = require('express'),
	fs = require('fs'),
	env = process.env.NODE_ENV || 'development',
	config = require('./config/config')[env],
	Mongoose = require('mongoose');

var connect = function() {
	var options = { server: { socketOptions: { keepAlive: 1 } } }
	Mongoose.connect(config.mongo.url, options);
}
connect();

// Error handler
Mongoose.connection.on('error', function (err) {
	console.log(err);
});

// Reconnect when closed
Mongoose.connection.on('disconnected', function () {
	connect();
});

var models_path = __dirname + '/models'
fs.readdirSync(models_path).forEach(function (file) {
	if (~file.indexOf('.js')) require(models_path + '/' + file)
});

var app = express();
// express settings
require('./config/express')(app, config);

// Bootstrap routes
require('./config/routes')(app);

// Start the app by listening on <port>
var port = process.env.PORT || 3002;
app.listen(port);
console.log('Express app started on port '+port);

// expose app
exports = module.exports = app;